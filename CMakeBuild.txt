cmake_minimum_required (VERSION 2.8)
project(libsendao)

include_directories( "./include" )

export(PACKAGE sendao)

set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} "-std=c++11 -rdynamic")
file(GLOB srcode src/*cpp)
add_library(sendao STATIC SHARED ${srcode})

install(TARGETS sendao
	EXPORT sendao
	LIBRARY DESTINATION "lib"
	ARCHIVE DESTINATION "lib/static"
	)
install(DIRECTORY include/ DESTINATION include/sendao
	FILES_MATCHING PATTERN "*.h")
install(FILES sendaoConfig.cmake DESTINATION lib/cmake/sendao)
