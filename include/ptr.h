#ifndef __GLUTON_PTR_H
#define __GLUTON_PTR_H


typedef class Pointer Pointer;
typedef struct _mapstring mapstring;

// api facing types

class ptrmap
{
public:
	ptrmap();
	~ptrmap();
	DHashmap *byAddr;
	unsigned long long bytes_in_use;

public:
	Pointer *Get(void *);
	void Del( void * );
	Pointer *Save(void *, size_t size=0, sint32_t type=0, const char *name=NULL);
	void *Alloc(int sz);
	void *Realloc(void *, int);
	static uint32_t addrkey(void*);
	static uint64_t addrkey64(void*);
};


/*
 * strmap API
 *
 * Dynamic string deduplication
 *
 * newptr = GS->Inter( "Some Static String" );
 * // note: no need to Free(), Inter'd strings stick around
 *
 * newptr = GS->Inter
 *
 * newptr = GS->Read( oldptr );
 * GS->Free( newptr );
 *
 * newptr = GS->Read( "Some Static String" );
 * GS->Free( newptr ); // dereference after using a copy
 *
 *
 *
 */
class strmap
{
public:
	strmap();
	~strmap();
	DHashmap *byName;

public:
	// Read: consumes a string, freeing it, deduping it into map.
	char *Read( char* );
	// UnRead: does not increment refcounter past 1.
	char *UnRead( char* );

	// Copy: duplicates a string.
	char *Copy( const char* );
	// UnCopy: again, without incrementing refcounter past 1.
	char *UnCopy( const char* );

	// Free: Deref.
	void Free( char* );
	// Deref: Free.
	void Deref( char* );

	// Compare:
	bool Compare( const char *, const char * );

public: // internal:
	mapstring *Search( const char *Src, uint32_t idc=0 );
};


extern strmap *GS;
extern ptrmap *GP;

// internal types

class Pointer
{
public:
	Pointer();
	Pointer(sint32_t,void*);
	~Pointer();
	sint32_t type;
	void *addr;
	char *name;
	size_t sz;
	int count;

public:
	void SetName(const char *);
	// manage object parameters
//	Pointer *Get(const char *);
//	void Set(const char *, Pointer *);
};


struct _mapstring
{
	char *str;
	int count;
	size_t len;
};

#endif
