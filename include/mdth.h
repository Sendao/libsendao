#ifndef _SENDAO_MULTI_DIMENSIONAL_TREE_HASH
#define _SENDAO_MULTI_DIMENSIONAL_TREE_HASH

typedef class MDTH MDTH;
typedef struct _mdtree mdtree;

// The idea is basically a two-key indexing of values.
// put in 'angle' and 'key' and get values.
// Internally, each angle has its own tree.
//


class MDTH {
public:
	MDTH();
	MDTH(MDTH *);
	~MDTH();

public:


public:
	tlist *ToList(cloneFunction *cF=NULL, bool includeNode=false);

	void Angle( uint32_t angle );

	nktype *Get_(uint32_t idcode);
	void *Get(uint32_t idcode);
	void Set(uint32_t idcode, void *v);
	void Del(uint32_t idcode);
	bool Has( uint32_t idc );
};


struct _mdtree
{
	uint8_t mdtype;
	void *data;
	uint32_t size;
};

#endif
