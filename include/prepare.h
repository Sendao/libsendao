#ifndef _SENDAO_PREPARE
#define _SENDAO_PREPARE

typedef struct _prepare_task PrepareTask;

class Prepare
{
public:
	Prepare();
	~Prepare();

public: // internal, do not modify
	uint16_t topid;
	HTable *tasks;

public: // prepare
	uint16_t task(void *);
	void require(uint16_t taskid, uint16_t requireid);

public: // run
	void singlethread(void);
	void threaded(void);

};

struct _prepare_task
{
	uint16_t id;
	void *handler;
	tlist *reqs;
	tlist *preps;
};

#endif
