#ifndef _SENDAO_MAPS
#define _SENDAO_MAPS

/*
#### class DMap
Maps intvoid

#### class TMap
Maps inttype

#### class SMap
Maps namekeyvoid

#### class OMap
Maps namekeytype
*/


typedef class DMap DMap;
typedef class DMap64 DMap64;
typedef class SMap SMap;
typedef class XMap XMap;
typedef class DHash DHash;
typedef class DHashmap DHashmap;
typedef class DHashmap64 DHashmap64;
typedef class SDHash SDHash;
typedef class SDHashmap SDHashmap;

typedef struct _DeepHash DeepHash;
typedef struct _DeepStrHash DeepStrHash;
typedef struct _DeepHashMap DeepHashMap;
typedef struct _DeepHashMap64 DeepHashMap64;
typedef struct _DeepStrHashMap DeepStrHashMap;

class DMap : public HTable { // map ivoid
public:
	DMap(uint32_t granularity);
	virtual ~DMap();

	DMap(DMap *);
	void CopyFrom(HTable *src, keyFunction *vK=keyof_ivoid, cloneFunction *vC=clone_ivoid);
	void CopyFrom(tlist *src, keyFunction *vK=keyof_ivoid, cloneFunction *vC=clone_ivoid);
	tlist *ToList(cloneFunction *cF=NULL, bool copyNode=false, bool includeNode=false);
	void Foreach( voidFunction *cb );
	void Clear( voidFunction *cb=NULL );

	bool Remove( uint32_t idcode, void *v ) { // removes an item from
		tlist *refs = HTable::Search(idcode);
		tnode *n, *nn;
		ivoid *x;

		forTSLIST( x, n, refs, ivoid*, nn ) {
			if( x->idc == idcode && x->ptr == v ) {
				refs->Pull(n);
				free_ivoid(x);
				return true;
			}
		}
		return false;
	};

	tlist *Gets( uint32_t idcode );
	ivoid *Get_(uint32_t idcode);
	void *Get(uint32_t idcode);
	void Set(uint32_t idcode, void *v);
	void Del(uint32_t idcode);
	void Del(uint32_t idcode, void *idptr);
	bool Has( uint32_t idc );
};
class DMap64 : public HTable64 { // map ivoid64
public:
	DMap64(uint64_t granularity);
	virtual ~DMap64();

	DMap64(DMap64 *);
	void CopyFrom(HTable64 *src, keyFunction64 *vK=NULL, cloneFunction *vC=NULL);
	void CopyFrom(tlist *src, keyFunction64 *vK=NULL, cloneFunction *vC=NULL);
	tlist *ToList(cloneFunction *cF=NULL, bool copyNode=false, bool includeNode=false);
	void Foreach( voidFunction *cb );
	void Clear( voidFunction *cb=NULL );

	bool Remove( uint64_t idcode, void *v ) { // removes an item from
		tlist *refs = HTable64::Search(idcode);
		tnode *n, *nn;
		ivoid64 *x;

		forTSLIST( x, n, refs, ivoid64*, nn ) {
			if( x->idc == idcode && x->ptr == v ) {
				refs->Pull(n);
				free_ivoid64(x);
				return true;
			}
		}
		return false;
	};

	ivoid64 *Get_(uint64_t idcode);
	void *Get(uint64_t idcode);
	void Set(uint64_t idcode, void *v);
	void Del(uint64_t idcode);
	bool Has( uint64_t idc );
};


class SMap : public HTable { // map nkvoid
	public:
	SMap(uint32_t granularity);
	virtual ~SMap();

	SMap(SMap *);
	virtual void CopyFrom(HTable *src, keyFunction *vK=NULL, cloneFunction *vC=NULL);
	virtual void CopyFrom(tlist *src, keyFunction *vK=NULL, cloneFunction *vC=NULL);
	virtual tlist *ToList(cloneFunction *cF=NULL, bool copyNode=false, bool includeNode=false);
	virtual void Foreach( voidFunction *cb );
	void Clear( voidFunction *cb=NULL );

	bool Remove( uint32_t idcode, void *v ) {
		tlist *refs = HTable::Search(idcode);
		tnode *n, *nn;
		nkvoid *x;

		forTSLIST( x, n, refs, nkvoid*, nn ) {
			if( x->idc == idcode && x->ptr == v ) {
				refs->Pull(n);
				free_nkvoid(x);
				return true;
			}
		}
		return false;
	};

	nkvoid *Get_(uint32_t idcode);
	nkvoid *Get_(const char *s);
	void *Get(uint32_t idcode);
	void *Get(const char *s);
	void Set(const char *s, void *v);
	void Del( nkvoid * );
	void Del( uint32_t idcode );
	void Del(const char * );
};

struct _DeepHash {
	union {
		void *ptr;
		DeepHash **ptrs;
	} data;

	uint32_t key;
	bool deeper;
};

class DHash { // nest void
public:
	DHash(uint32_t _keymax, uint16_t _layermax);
	virtual ~DHash();

public:
	uint32_t keymax;
	uint16_t layermax;
	uint8_t maxdepth;

	DeepHash top;
	uint16_t count;
	uint32_t *layerkeys, *keyrates;

	tlist *ToList(cloneFunction *cF=NULL);
	virtual void Foreach( voidFunction *cb );

	void *Get(uint32_t idcode);
	void Set(uint32_t idcode, void *v);
	void Del(uint32_t idcode);
	bool Has( uint32_t idc );
	void Clear(void);

public://notapi
	DeepHash *_Get(uint32_t idc, uint8_t *depth);
};

struct _DeepHashMap {
	union {
		tlist *vals;
		DeepHashMap **ptrs;
	} data;

	uint32_t key;
	bool deeper;
};

class DHashmap { // nest void
public:
	DHashmap(uint32_t _keymax, uint16_t _layermax);
	virtual ~DHashmap();

public:
	uint32_t keymax;
	uint16_t layermax;
	uint8_t maxdepth;

	DeepHashMap top;
	uint16_t count;
	uint32_t *layerkeys, *keyrates;
	bool usedivmod;

	tlist *ToList();//cloneFunction *cF=NULL, bool includeNode=false);
	tlist *Get(uint32_t idcode);
	tlist *GetRange(uint32_t idc0, uint32_t idc1);
	void Add(uint32_t idcode, void *v);
	void Del(uint32_t idcode, void *v=NULL);
	bool Has( uint32_t idc );
	void Clear(voidFunction *vF=NULL);
public:
	uint32_t Keymod( uint32_t idcode, uint32_t realmax=0, uint32_t mymax=0 );
	uint32_t KeymodHigh( uint32_t idcode, uint32_t realmax=0, uint32_t mymax=0 );
	uint32_t pointerkey( void *ptr );

public://notapi
	DeepHashMap *_Get(uint32_t idc, uint8_t *depth);
};

struct _DeepHashMap64 {
	union {
		tlist *vals;
		DeepHashMap64 **ptrs;
	} data;

	uint64_t key;
	bool deeper;
};

class DHashmap64 { // nest void
public:
	DHashmap64(uint64_t _keymax, uint32_t _layermax);
	virtual ~DHashmap64();

public:
	uint64_t keymax;
	uint32_t layermax;
	uint8_t maxdepth;

	DeepHashMap64 top;
	uint16_t count;
	uint64_t *layerkeys, *keyrates;
	bool usedivmod;

	tlist *ToList();//cloneFunction *cF=NULL, bool includeNode=false);
	tlist *Get(uint64_t idcode);
	tlist *GetRange(uint64_t idc0, uint64_t idc1);
	void Add(uint64_t idcode, void *v);
	void Del(uint64_t idcode, void *v=NULL);
	bool Has( uint64_t idc );
	void Clear(voidFunction *vF=NULL);
public:
	uint64_t Keymod( uint64_t idcode, uint64_t realmax=0, uint64_t mymax=0 );
	uint64_t KeymodHigh( uint64_t idcode, uint64_t realmax=0, uint64_t mymax=0 );
	uint64_t LayerKey(uint64_t idc, int layern);
	uint64_t pointerkey( void *ptr );

public://notapi
	DeepHashMap64 *_Get(uint64_t idc, uint8_t *depth);
};



struct _DeepStrHash {
	union {
		void *ptr;
		DeepStrHash **ptrs;
	} data;

	uint32_t key;
	char *skey;
	bool deeper;
};


class SDHash { // nest void
public:
	SDHash(uint32_t _keymax, uint16_t _layermax);
	virtual ~SDHash();

public:
	uint32_t keymax;
	uint16_t layermax;
	uint8_t maxdepth;

	DeepStrHash top;
	uint16_t count;
	uint32_t *layerkeys, *keyrates;

	tlist *ToList(cloneFunction *cF=NULL, bool includeNode=false);

	void *Get(const char *key);
	void Set(const char *key, void *v);
	void Del(const char *key);
	bool Has(const char *key);
	void Clear(void);

public://notapi
	DeepStrHash *_Get(uint32_t idc, const char *key, uint8_t *depth );
};

struct _DeepStrHashMap {
	union {
		tlist *vals;
		DeepStrHashMap **ptrs;
	} data;

	uint32_t key;
	char *skey;
	bool deeper;
};

class SDHashmap { // nest void
public:
	SDHashmap(uint32_t _keymax, uint16_t _layermax);
	virtual ~SDHashmap();

public:
	uint32_t keymax;
	uint16_t layermax;
	uint8_t maxdepth;

	DeepStrHashMap top;
	uint16_t count;
	uint32_t *layerkeys, *keyrates;

	tlist *ToList(cloneFunction *cF=NULL, bool includeNode=false);

	tlist *Get(const char *key);
	void Add(const char *key, void *v);
	void Del(const char *key, void *v);
	bool Has(const char *key);
	void Clear(voidFunction *vF=NULL);

public://notapi
	DeepStrHashMap *_Get(const char *key, uint32_t idc, uint8_t *depth);
};


class XMap : public HTable { // map ivoid
public:
	XMap();
	virtual ~XMap();

	XMap(XMap *);
	virtual void CopyFrom(HTable *src, keyFunction *vK=NULL, cloneFunction *vC=NULL);
	virtual void CopyFrom(tlist *src, keyFunction *vK=NULL, cloneFunction *vC=NULL);
	tlist *ToList(cloneFunction *cF=NULL, bool includeNode=false);

	bool Remove( uint32_t idcode, void *v ) { // removes an item from
		tlist *refs = HTable::Search(idcode);
		tnode *n, *nn;
		ivoid *x;

		forTSLIST( x, n, refs, ivoid*, nn ) {
			if( x->idc == idcode && x->ptr == v ) {
				refs->Pull(n);
				free_ivoid(x);
				return true;
			}
		}
		return false;
	};

	ivoid *Get_(uint32_t idcode);
	void *Get(uint32_t idcode);
	void Set(uint32_t idcode, void *v);
	void Del(uint32_t idcode);
	bool Has( uint32_t idc );
};


#endif
