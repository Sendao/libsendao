#ifndef __SENDAO_TREE
#define __SENDAO_TREE


typedef class HotTree HotTree;
typedef class Tree Tree;
typedef class TreeBranch TreeBranch;
//typedef class TreeBranch TreeNode;

typedef sint8_t tree_branch_func( Tree *tree, TreeBranch *branch, void *ptr );

class Tree
{
public:
	Tree(sint8_t _keys);
	~Tree();

public:
	TreeBranch *trunk;
	sint8_t keys;
	uint64_t count;
	tree_branch_func *branchfunc;

	void Assert(void); // verify inputs
	bool valid;
	void *data;

public:
	sint8_t Compare( TreeBranch *from, void *to );
	tlist *Range( void *pBefore, void *pAfter );
	TreeBranch *Seek( void *ptr, tree_branch_func *custom_bf = NULL );
	TreeBranch *Seek( TreeBranch *from, void *ptr, tree_branch_func *custom_bf = NULL );
	void Add( void *ptr );
	void Remove( void *ptr );
	void Clear(voidFunction *vF=NULL);
	void ForeachStacks(voidFunction*);
};


class TreeBranch
{
public:
	TreeBranch(Tree *ofTree);
	~TreeBranch();

public:
	void *data;
	TreeBranch **forks;
	TreeBranch *parent;
	uint32_t depth;

public:
	void Add( void *ptr, sint8_t branchkey );
};

class HotTree : Tree
{
public:
	HotTree(Hot *_hot, sint8_t _keys);
	~HotTree();

	Hot *hot;

};

#endif
