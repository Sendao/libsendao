#ifndef __SENDAO_TABLES_H
#define __SENDAO_TABLES_H

uint32_t key3d( int x, int y, int z, int mx, int my, int mz );


class HTable {
	public:
//	static sint32_t Register();
//	static sint32_t registryid;
	HTable(uint32_t kmax);
	virtual ~HTable();
	void Clear( voidFunction *vC=NULL );
	void ClearWith( void2Function *vC, sint32_t data );
	virtual void CopyFrom( HTable *src, keyFunction *vK, cloneFunction *vC );
	virtual void CopyFrom( tlist *src, keyFunction *vK, cloneFunction *vC );
	virtual tlist *ToList( cloneFunction *vC=NULL, bool pointerClone=false );
	virtual tlist *Slice( uint32_t i0=0, uint32_t imax=0, cloneFunction *vC=NULL, bool pointerClone=false );

	tlist *Search(uint32_t key);

	bool Remove(uint32_t key, void *ptr);
	bool Has(uint32_t key, void *ptr);
	void Add(uint32_t key, void *ptr);
	void AddList( uint32_t key, tlist *items );
	void SetRange(uint32_t kmax);

	void Foreach( void cb(void*) );
	void Foreach( uint32_t key, void cb(void*) );

	public:
	tlist **tab;
	uint32_t keymax;
	uint32_t usedkeys;
};


class HTable64 {
	public:
	HTable64(uint64_t kmax);
	virtual ~HTable64();
	void Clear( voidFunction *vC=NULL );
	void CopyFrom( HTable64 *src, keyFunction64 *vK, cloneFunction *vC );
	void CopyFrom( tlist *src, keyFunction64 *vK, cloneFunction *vC );
	virtual tlist *ToList( cloneFunction *vC=NULL, bool pointerClone=false );
	virtual tlist *Slice( uint64_t i0=0, uint64_t imax=0, cloneFunction *vC=NULL, bool pointerClone=false );

	tlist *Search(uint64_t key);

	bool Remove(uint64_t key, void *ptr);
	void Add(uint64_t key, void *ptr);
	void AddList( uint64_t key, tlist *items );
	void SetRange(uint64_t kmax);

	void Foreach( void cb(void*) );
	void Foreach( uint64_t key, void cb(void*) );

	public:
	tlist **tab;
	uint64_t keymax;
	uint64_t usedkeys;
};

#define forHTABLE( var, listiter, listctr, tbl, type ) \
	for( (listctr) = 0; (listctr) < (tbl)->keymax; (listctr)++ ) \
		if( (tbl)->tab[(listctr)] ) \
			for( (listiter) = (tbl)->tab[(listctr)]->nodes; (listiter); (listiter) = (listiter)->next ) \
				if( ( (var) = (type)((listiter)->data) ) == NULL )

#endif
