#ifndef _SENDAO_MEM_H
#define _SENDAO_MEM_H


typedef struct _quick_list		quick_list;
typedef struct _mem_trace			mem_trace;
typedef struct _mem_track			mem_track;

class PointerBuffer
{
public:
	PointerBuffer(size_t _unit, bool _tracing=false);
	~PointerBuffer();

public: // settings (set before using)
	uint32_t chunk; // how many items per buffer
	uint32_t pubunit;// how many bytes per item
	uint32_t unit;
	bool tracing;

public: // internals (do not modify)
	uint64_t allocations; // current progress counter; reset by reportMem()
	uint64_t alloced; // total count
	quick_list *chunks; // each one is unit*chunk in size
	quick_list *freeGroups, *freeSingles;
	DHashmap64 *tGroups, *tSingles;
	timestop_v Schunks, Ssingles, Sgroups, Salloc;
	bool tables_main;
	uint32_t chunkused; // how much of the current chunk has been used
	void _Alloc(void);
	void *_Alloc( uint32_t number );
	void *_AllocOne( uint32_t number );
	void releaseRemainingChunk( void );
	void *splitFreeGroup( uint32_t number );

public:
	void Defrag(void);
	void Defrag(void *ch);
	void* Create( void );
	void* Create( uint32_t  number );
	void* CreateActualSize( size_t sz );
	void Release( void* ); // decrease refcount and deallocate
	void Reuse( void *ptr ); // increase refcount
	void ReleaseAll( void ); // release all allocations and reset pointers
};

struct _mem_trace {
	uint8_t refs;
	uint32_t sz;
	bool istracer;
	void **alloctrace;
	int alloccount;
	void **freetrace;
	int freecount;
	time_t allocTime;
};

struct _mem_track {
	uint8_t refs;
	uint32_t sz;
	bool istracer;
};

struct _quick_list {
	void *ptr;
	quick_list *next;
	quick_list *prev;
};
quick_list *quicklist(void*, quick_list*);
extern PointerBuffer **mainMemory;
void initMemory(int numSlots=8, int *in_sizes=NULL, int *in_chunks=NULL, bool *in_trace=NULL );
void exitMemory( void );
void prepMemory( int slotSize, int slotChunks=24, bool doTrace=false );
void defragMem(void);
char *reportMem(void);
char *reportMem(PointerBuffer*);


#endif
