#include "sendao.h"

DMap::DMap( uint32_t gran ) : HTable(gran)
{
}

DMap::~DMap()
{
	Clear( );
}

void DMap::CopyFrom( HTable *src, keyFunction *vK, cloneFunction *vC )
{
	HTable::CopyFrom( src, vK ? vK : keyof_ivoid, vC ? vC : clone_ivoid );
}
void DMap::CopyFrom( tlist *src, keyFunction *vK, cloneFunction *vC )
{
	HTable::CopyFrom( src, vK ? vK : keyof_ivoid, vC ? vC : clone_ivoid );
}

tlist *DMap::ToList( cloneFunction *vC, bool copyNode, bool includeNode )
{
	tlist *nx = new tlist;
	tnode *n;
	ivoid *p, *p2;
	void *px;
	uint32_t i;

	for( i = 0; i < keymax; i++ ) {
		if( tab[i] ) {
			for( n = tab[i]->nodes; n; n = n->next ) {
				p = (ivoid*)n->data;
				if( vC ) {
					if( includeNode || copyNode ) {
						nx->PushBack( vC( p ) );
					} else if( !p->ptr ) {
						continue;
					} else {
						px = vC(p->ptr);
						p2 = new_ivoid(p->idc, px);
						nx->PushBack( (void*)p2 );
					}
				} else if( includeNode ) {
					nx->PushBack( p );
				} else if( copyNode ) {
					nx->PushBack( clone_ivoid( p ) );
				} else {
					nx->PushBack( p->ptr );
				}
			}
		}
	}

	return nx;
}


tlist *DMap::Gets( uint32_t idc )
{
	tlist *l = HTable::Search(idc);
	tnode *n;
	ivoid *iv;

	tlist *results = new tlist;

	forTLIST( iv, n, l, ivoid* ) {
		if( iv->idc == idc ) {
			results->Push(iv->ptr);
		}
	}

	return results;
}

ivoid *DMap::Get_( uint32_t idc )
{
	tlist *l = HTable::Search(idc);
	tnode *n;
	ivoid *iv;

	forTLIST( iv, n, l, ivoid* ) {
		if( iv->idc == idc ) {
			return iv;
		}
	}

	return NULL;
}
void *DMap::Get( uint32_t idc )
{
	ivoid *x = Get_(idc);
	return x ? x->ptr : NULL;
}

void DMap::Set( uint32_t idc, void *v )
{
	ivoid *iv = new_ivoid(idc,v);
	HTable::Add(idc,iv);
}

bool DMap::Has( uint32_t idc )
{
	tlist *l = HTable::Search(idc);
	tnode *n;
	ivoid *iv;

	forTLIST( iv, n, l, ivoid* ) {
		if( iv->idc == idc ) {
			return true;
		}
	}
	return false;
}

void DMap::Clear( voidFunction *cb )
{
	if( cb ) Foreach(cb);
	HTable::Clear((voidFunction*)free_ivoid);
}

void DMap::Foreach( voidFunction *cb )
{
	uint32_t i;
	tnode *n, *next;
	ivoid *iv;

	for( i=0; i<keymax; i++ ) {
		if( !tab[i] ) continue;
		for( n = tab[i]->nodes; n; n = next ) {
			next = n->next;
			iv = (ivoid*)n->data;
			cb( iv->ptr );
		}
	}
}


void DMap::Del( uint32_t idc )
{
	tlist *l = HTable::Search(idc);
	tnode *n, *nn;
	ivoid *iv;

	forTSLIST( iv, n, l, ivoid*, nn ) {
		if( iv->idc == idc ) {
			l->Pull(n);
			free_ivoid(iv);
		}
	}

	if( l && l->count == 0 ) {
		deleteMem(l);

		tab[idc%keymax]=NULL;
		usedkeys--;
	}
}
void DMap::Del( uint32_t idc, void *idptr )
{
	tlist *l = HTable::Search(idc);
	tnode *n, *nn;
	ivoid *iv;

	forTSLIST( iv, n, l, ivoid*, nn ) {
		if( iv->idc == idc && iv->ptr == idptr ) {
			l->Pull(n);
			free_ivoid(iv);
		}
	}

	if( l && l->count == 0 ) {
		deleteMem(l);

		tab[idc%keymax]=NULL;
		usedkeys--;
	}
}



SMap::SMap( uint32_t gran ) : HTable(gran)
{
}

SMap::~SMap()
{
	Clear();// (voidFunction*)free_nkvoid );
}

void SMap::CopyFrom( HTable *src, keyFunction *vK, cloneFunction *vC )
{
	HTable::CopyFrom( src, vK ? vK : keyof_nkvoid, vC ? vC : clone_nkvoid );
}
void SMap::CopyFrom( tlist *src, keyFunction *vK, cloneFunction *vC )
{
	HTable::CopyFrom( src, vK ? vK : keyof_nkvoid, vC ? vC : clone_nkvoid );
}

tlist *SMap::ToList( cloneFunction *vC, bool copyNode, bool includeNode )
{
	tlist *nx = new tlist;
	tnode *n;
	nkvoid *p, *p2;
	void *px;
	uint32_t i;

	for( i = 0; i < keymax; i++ ) {
		if( tab[i] ) {
			for( n = tab[i]->nodes; n; n = n->next ) {
				p = (nkvoid*)n->data;
				if( vC ) {
					if( includeNode || copyNode ) {
						nx->PushBack( vC(p) );
					} else if( !p->ptr ) {
						continue;
					} else {
						px = vC(p->ptr);
						p2 = new_nkvoid(p->name, p->idc, px);
						nx->PushBack( (void*)p2 );
					}
				} else if( includeNode ) {
					nx->PushBack( p );
				} else if( includeNode ) {
					nx->PushBack( clone_nkvoid( p ) );
				} else {
					nx->PushBack( p->ptr );
				}
			}
		}
	}

	return nx;
}

nkvoid *SMap::Get_( uint32_t idc )
{
	tlist *l = HTable::Search(idc);
	tnode *n;
	nkvoid *iv;

	forTLIST( iv, n, l, nkvoid* ) {
		if( iv->idc == idc ) {
			return iv;
		}
	}

	return NULL;
}
void *SMap::Get( uint32_t idc )
{
	nkvoid *x = Get_(idc);
	return x ? x->ptr : NULL;
}

nkvoid *SMap::Get_( const char *s )
{
	uint32_t idc = namekey(s);
	tlist *l = HTable::Search(idc);
	tnode *n;
	nkvoid *iv;

	forTLIST( iv, n, l, nkvoid* ) {
		if( iv->idc == idc && str_c_cmp( iv->name, s ) == 0 ) {
			return iv;
		}
	}

	return NULL;
}
void *SMap::Get( const char *s )
{
	nkvoid *x = Get_(s);
	return x ? x->ptr : NULL;
}

void SMap::Set( const char *s, void *v )
{
	uint32_t idc = namekey(s);
	nkvoid *iv = new_nkvoid(s,idc,v);
	HTable::Add(idc,iv);
}

void SMap::Clear( voidFunction *cb )
{
	if( cb ) Foreach(cb);
	HTable::Clear((voidFunction*)free_nkvoid);
}

void SMap::Foreach( voidFunction *cb )
{
	uint32_t i;
	tnode *n, *next;
	nkvoid *iv;

	for( i=0; i<keymax; i++ ) {
		if( !tab[i] ) continue;
		for( n = tab[i]->nodes; n; n = next ) {
			next = n->next;
			iv = (nkvoid*)n->data;
			cb( iv->ptr );
		}
	}
}

void SMap::Del( uint32_t idc )
{
	tlist *l = HTable::Search(idc);
	tnode *n, *nn;
	nkvoid *iv;

	forTSLIST( iv, n, l, nkvoid*, nn ) {
		if( iv->idc == idc ) {
			l->Pull(n);
			free_nkvoid(iv);
		}
	}

	if( l && l->count == 0 ) {
		deleteMem(l);

		tab[idc%keymax]=NULL;
		usedkeys--;
	}
}

void SMap::Del( const char *s )
{
	uint32_t idc = namekey(s);
	tlist *l = HTable::Search(idc);
	tnode *n, *nn;
	nkvoid *iv;

	forTSLIST( iv, n, l, nkvoid*, nn ) {
		if( iv->idc == idc && str_c_cmp( iv->name, s ) == 0 ) {
			l->Pull(n);
			free_nkvoid(iv);
		}
	}

	if( l && l->count == 0 ) {
		deleteMem(l);

		tab[idc%keymax]=NULL;
		usedkeys--;
	}
}

void SMap::Del( nkvoid *ptr )
{
	uint32_t idc = ptr->idc;
	tlist *l = HTable::Search( idc );
	tnode *n, *nn;
	nkvoid *iv;

	forTSLIST( iv, n, l, nkvoid*, nn ) {
		if( iv == ptr ) {
			l->Pull(n);
			free_nkvoid(iv);
		}
	}

	if( l && l->count == 0 ) {
		deleteMem(l);

		tab[idc%keymax]=NULL;
		usedkeys--;
	}
}
