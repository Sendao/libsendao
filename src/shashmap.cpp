#include "sendao.h"


DeepStrHashMap *new_deepstrhashmap(void)
{
	DeepStrHashMap *p = (DeepStrHashMap*)grabMem(sizeof(DeepStrHashMap));
	p->data.ptrs = NULL;
	p->key = 0;
	p->skey = NULL;
	p->deeper = false;
	return p;
}
DeepStrHashMap *new_deepstrhashmap( const char *skey, uint32_t idc, void *v )
{
	DeepStrHashMap *result = new_deepstrhashmap();
	result->data.vals = new tlist();
	result->data.vals->Push(v);
	result->key = idc;
	result->skey = GS->Copy(skey);
	result->deeper = false;
	return result;
}

DeepStrHashMap *new_deepstrhashmap( char *skey, uint32_t idc, tlist *vsrc )
{
	DeepStrHashMap *result = new_deepstrhashmap();
	result->data.vals = vsrc;
	result->key = idc;
	result->skey = skey;
	result->deeper = false;
	return result;
}

void free_deepstrhashmap(DeepStrHashMap *x, uint8_t layermax)
{
	uint8_t n;
	Recursor rc;

	rc.Push(x);
	while( (x=(DeepStrHashMap*)rc.Iterate()) ) {

		if( x->deeper ) {
			for(n=0;n<layermax;n++){
				rc.Push( x->data.ptrs[n] );
			}
		} else {
			if( x->data.vals != NULL ) {
				x->data.vals->Clear();
				deleteMem(x->data.vals);

			}
		}
		GS->Free( x->skey );
		releaseMem(x);
	}
}

SDHashmap::SDHashmap( uint32_t _keymax, uint16_t _layermax )
{
	keymax = _keymax;
	layermax = _layermax;

	uint32_t vx = keymax/layermax;
	for( maxdepth = 0; vx > layermax; vx /= layermax, maxdepth++ );

	maxdepth++;

	keyrates = (uint32_t*)grabMem(sizeof(uint32_t)*(maxdepth+1));

	uint8_t n = maxdepth;
	keyrates[n] = 1;
	do {
		--n;
		keyrates[n] = vx;
		vx *= layermax;
	} while( n != 0 );

	if( vx != keymax ) {
		fprintf(stderr, "Error: rebuilding for dhash did not work as expected (%u/%u)", vx, keymax);
	}

	top.data.ptrs = NULL;
	top.deeper = false;
	top.key = 0;
}

SDHashmap::~SDHashmap()
{
	Clear();
}

void SDHashmap::Clear(voidFunction *vF)
{
	uint8_t n;

	if( !top.deeper ) {

		if( top.data.vals ) {
			top.data.vals->Clear(vF);
			deleteMem(top.data.vals);

			top.data.vals = NULL;
		}

		return;
	}

	for( n=0; n<layermax; ++n ) {
		free_deepstrhashmap( top.data.ptrs[n], layermax );
	}
	releaseMem( top.data.ptrs );
	top.data.ptrs = NULL;
	top.deeper = false;
}

DeepStrHashMap *SDHashmap::_Get( const char *skey, uint32_t idc, uint8_t *depth )
{
	uint32_t layerkey, lastkey=idc;
	DeepStrHashMap *i, *j;

	*depth = 0;
	for( i = &top; i; i = j ) {
		if( (*depth) >= maxdepth ) {
			return i;
		}

		layerkey = lastkey / keyrates[*depth];
		lastkey = idc % keyrates[*depth];

		if( !i->deeper ) {
			return i;
		}

		j = i->data.ptrs[ layerkey ];
		if( !j ) {
			return i;
		}

		(*depth)++;
	}

	throw "invalid code marker 2387491";
}
tlist *SDHashmap::Get( const char *skey )
{
	uint32_t idc = namekey(skey);
	uint8_t depth;
	DeepStrHashMap *x = _Get(skey, idc, &depth);

	if( x->key == idc && GS->Compare(skey, x->skey) )
		return x->data.vals;
	return NULL;
}


void SDHashmap::Add( const char *skey, void *v )
{
	uint32_t idc = namekey(skey);
	uint8_t depth, layern, oldlayern;
	uint32_t ratekey, oldratekey;
	DeepStrHashMap *result=NULL, *y, *x = _Get(skey, idc, &depth);

	if( !x ) {
		fprintf(stderr,"dhash is full?\n");
		throw "dhash shouldn't fill.";
	}

	if( !x->deeper ) {
		if( x->data.vals == NULL ) {
			x->data.vals = new tlist;
			x->key = idc;
			x->skey = GS->Copy(skey);
		}
		if( x->key == idc && GS->Compare(x->skey, skey) ) {
			x->data.vals->PushBack(v);
			return;
		}
		if( x->key == idc ) {
			//they both exist in the same space! ... well... ok. HAHAHAHAHAHA //!fixme
			x->data.vals->PushBack(v);
			return;
		}
	}

	result = new_deepstrhashmap(skey,idc,v);

	if( x->deeper ) {
		ratekey = idc % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		layern = ratekey / keyrates[depth];
		x->data.ptrs[ layern ] = result;
		return;
	}

	y=x;
	do {
		x = y;
		y = new_deepstrhashmap(); // x->key,x->data.vals);
		y->key = x->key;
		y->skey = x->skey;
		y->data.vals = x->data.vals;

		x->deeper = true;

		x->data.ptrs = (DeepStrHashMap**)grabMem(sizeof(DeepStrHashMap*)*layermax);
		memset( x->data.ptrs, 0, sizeof(DeepStrHashMap*)*layermax );

		oldratekey = y->key % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		oldlayern = oldratekey / keyrates[depth];
		x->data.ptrs[ oldlayern ] = y;

		ratekey = idc % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		layern = ratekey / keyrates[depth];

		depth++;
	} while( layern == oldlayern );

	x->data.ptrs[ layern ] = result;
}

bool SDHashmap::Has( const char *skey )
{
	uint32_t idc = namekey(skey);
	uint8_t depth;
	DeepStrHashMap *x = _Get(skey,idc,&depth);
	if( x && !x->deeper && x->key == idc /* && GS->Compare(skey, x->key) */ ) return true;
	return false;
}

void SDHashmap::Del( const char *skey, void *v )
{
	uint32_t idc = namekey(skey);
	uint8_t depth;
	uint32_t layerkey, lastkey=idc;
	DeepStrHashMap *i, *j;

	depth = 0;
	for( i = &top; i; i = j ) {
		if( !i->deeper ) {
			return;
		}
		if( depth > maxdepth ) {
			fprintf(stderr, "maxdepth exception?\n");
			return;
		}

		layerkey = lastkey / keyrates[depth];
		lastkey = idc % keyrates[depth];

		j = i->data.ptrs[ layerkey ];
		if( !j ) return;

		if( !j->deeper && j->key == idc ) {

			tnode *n;

			for( n = j->data.vals->nodes; n; n = n->next ) {
				if( n->data == v ) {
					j->data.vals->Pull( n );
					break;
				}
			}

			if( j->data.vals->count <= 0 ) {
				i->data.ptrs[layerkey] = NULL;
				free_deepstrhashmap(j, layermax);
			}

			return;
		}

		depth++;
	}

	throw "invalid code marker 2387499";
}
