#include "sendao.h"


DeepHashMap64 *new_deephashmap64(void)
{
	DeepHashMap64 *p = (DeepHashMap64*)grabMem(sizeof(DeepHashMap64));
	p->data.ptrs = NULL;
	p->key = 0;
	p->deeper = false;
	return p;
}
DeepHashMap64 *new_deephashmap64( uint64_t idc, void *v )
{
	DeepHashMap64 *result = new_deephashmap64();
	result->data.vals = new tlist();
	result->data.vals->Push(v);
	result->key = idc;
	result->deeper = false;
	return result;
}

DeepHashMap64 *new_deephashmap64( uint64_t idc, tlist *vsrc )
{
	DeepHashMap64 *result = new_deephashmap64();
	result->data.vals = vsrc;
	result->key = idc;
	result->deeper = false;
	return result;
}

void free_deephashmap64(DeepHashMap64 *x, uint8_t layermax)
{
	uint8_t n;
	Recursor rc;

	rc.Push(x);
	while( (x=(DeepHashMap64*)rc.Iterate()) ) {

		if( x->deeper ) {
			for(n=0;n<layermax;n++){
				if( x->data.ptrs[n] )
					rc.Push( x->data.ptrs[n] );
			}
			deleteMem( x->data.ptrs );
		} else {
			if( x->data.vals != NULL ) {
				x->data.vals->Clear();
				deleteMem(x->data.vals);
			}
		}
		releaseMem(x);
	}
}

DHashmap64::DHashmap64( uint64_t _keymax, uint32_t _layermax )
{
	keymax = _keymax;
	layermax = _layermax;
	usedivmod = false;

	uint64_t vx = keymax/(uint64_t)layermax;
	for( maxdepth = 0; vx > (uint64_t)layermax; vx /= (uint64_t)layermax, maxdepth++ );
	//fprintf(stderr, "start vx=%llu\n", vx);

	maxdepth++;

	keyrates = (uint64_t*)grabMem(sizeof(uint64_t)*(maxdepth+1));

	uint8_t n = maxdepth;
	keyrates[n] = 1;
	do {
		--n;
		keyrates[n] = vx;
		vx *= layermax;
		//fprintf(stderr, "vx=%llu\n", vx);
	} while( n != 0 );

	if( vx != keymax ) {
		fprintf(stderr, "Error: rebuilding for dhash did not work as expected md=%d, (%llu/%llu)\n", maxdepth, vx, keymax);
		fflush(stderr);
	}

	top.data.ptrs = NULL;
	top.deeper = false;
	top.key = 0;
}

DHashmap64::~DHashmap64()
{
	Clear();
	releaseMem(keyrates);
}

void DHashmap64::Clear(voidFunction *vF)
{
	uint8_t n;

	if( !top.deeper ) {

		if( top.data.vals ) {
			top.data.vals->Clear(vF);
			deleteMem(top.data.vals);

			top.data.vals = NULL;
		}

		return;
	}

	for( n=0; n<layermax; ++n ) {
		free_deephashmap64( top.data.ptrs[n], layermax );
	}
	releaseMem( top.data.ptrs );
	top.data.ptrs = NULL;
	top.deeper = false;
}

tlist *DHashmap64::ToList()
{
	DeepHashMap64 *i;
	uint8_t n;
	tlist *results = new tlist;
	Recursor r;

	r.Push(&top);
	//fprintf(stdout, "Processing dhm::tolist %p\n", &top);

	while( (i=(DeepHashMap64*)r.Iterate()) ) {

		if( i->deeper ) {
			for( n=0; n<layermax; ++n ) {
				if( i->data.ptrs[n] )
					r.Push( i->data.ptrs[n] );
			}
		} else {
			//fprintf(stdout, "Add data from %p %d\n", i->data.vals, i->data.vals->count );
			results->Append( i->data.vals );
		}

	}
	//fprintf(stdout, "found results->count = %d\n", results->count);

	return results;
}

uint64_t DHashmap64::LayerKey( uint64_t idc, int layern )
{
	uint64_t layerkey, lastkey, ckey=idc;
	int i=0;

	//fprintf(stdout, "LK(%llu/%d)\n", idc,layern);
	do {
		if( i <= 1 ) lastkey = Keymod(ckey, i==0?0:keymax, i==0?keymax:keyrates[0]);
		else lastkey = Keymod(ckey, keyrates[i-2], keyrates[i-1]);
		layerkey = floor( (float)lastkey / (i<maxdepth?(float)keyrates[i]:1) );
		ckey = ckey - layerkey * keyrates[i-1];
		//fprintf(stdout, "LK(%d)=%llu lastkey=%llu, layermax=%llu (%llu)\n", i, layerkey, lastkey, keyrates[i-1], ckey);
		i++;
	} while( i <= layern );

	fflush(stdout);

	return layerkey;
}

DeepHashMap64 *DHashmap64::_Get( uint64_t idc, uint8_t *depth )
{
	uint64_t layerkey, lastkey, ckey=idc;
	DeepHashMap64 *i, *j;

	*depth = 0;
	for( i = &top; i; i = j ) {
		if( !i->deeper )
			return i;

		if( *depth <= 1 ) lastkey = Keymod(ckey, *depth==0?0:keymax, *depth==0?keymax:keyrates[0]);
		else lastkey = Keymod(ckey, keyrates[*depth-2], keyrates[*depth-1]);
		layerkey = floor( (float)lastkey / (*depth<maxdepth?(float)keyrates[*depth]:1) );
		ckey = ckey - layerkey * keyrates[*depth-1];

		//fprintf(stdout, "Depth %d source %lu layer %lu last %lu\n", *depth, idc, layerkey, lastkey);
		//fflush(stdout);

		j = i->data.ptrs[ layerkey ];
		if( !j )
			return i;

		*depth = *depth + 1;
	}

	throw "invalid code marker 2387491";
}
tlist *DHashmap64::Get( uint64_t idc )
{
	uint8_t depth;
	DeepHashMap64 *x = _Get(idc, &depth);
	if( x && x->key == idc )
		return x->data.vals;
	//fprintf(stdout, "Entry has wrong key: %lu/%lu (Depth=%d)\n", x->key, idc, depth);
	//if( x->data.vals )
	//	fprintf(stdout, "%d entries\n", x->data.vals->count);
	//fflush(stdout);
	return NULL;
}
tlist *DHashmap64::GetRange( uint64_t idc0, uint64_t idc1 )
{
	tlist *results = new tlist;
	uint8_t depth;
	uint64_t layerkey, lastkey, ckey=idc0, layerkey2, lastkey2, ckey2=idc1;
	uint64_t tkey, rejectCount=0, branchCount=0;
	DeepHashMap64 *pTrack, *pNext;
	Recursor *r = new Recursor();

	if( !top.deeper ) {
		if( top.data.vals )
			results->Append( top.data.vals );
		deleteMem(r);
		return results;
	}

	depth = 0;
	pNext=NULL;
	for( pTrack = &top; pTrack ; pTrack = pNext ) {
		if( depth > maxdepth ) {
			fprintf(stdout, "Max depth exceeded in GetRange search.\n");
			fflush(stdout);
			break;
		}

			if( depth <= 1 ) lastkey = Keymod(ckey, depth==0?0:keymax, depth==0?keymax:keyrates[0]);
			else lastkey = Keymod(ckey, keyrates[depth-2], keyrates[depth-1]);
			layerkey = floor( (float)lastkey / (depth<maxdepth?(float)keyrates[depth]:1) );
			ckey = ckey - layerkey * keyrates[depth-1];

			if( depth <= 1 ) lastkey2 = Keymod(ckey2, depth==0?0:keymax, depth==0?keymax:keyrates[0]);
			else lastkey2 = Keymod(ckey2, keyrates[depth-2], keyrates[depth-1]);
			layerkey2 = floor( (float)lastkey2 / (depth<maxdepth?(float)keyrates[depth]:1) );
			ckey2 = ckey2 - layerkey2 * keyrates[depth-1];


/*
		layerkey = floor( (float)lastkey / (float)keyrates[depth] );
		layerkey2 = floor( (float)lastkey2 / (float)keyrates[depth] );


		ckey = ckey - layerkey * keyrates[depth];
		ckey2 = ckey2 - layerkey2 * keyrates[depth];

		lastkey = Keymod(ckey, depth==0?keymax:keyrates[depth-1], keyrates[depth]);
		lastkey2 = KeymodHigh(ckey2, depth==0?keymax:keyrates[depth-1], keyrates[depth]);

*/

		if( !pTrack->deeper ) {
			if( pTrack->key >= idc0 && pTrack->key <= idc1 ) {
				results->Append( pTrack->data.vals );
				deleteMem(r);
				return results;
			}
		}
		if( layerkey != layerkey2 ) {
			/*
			fprintf(stdout, "GR: last=%lu layer=%lu\n", lastkey, layerkey);
			fprintf(stdout, "GRX: last=%lu layer=%lu\n", lastkey2, layerkey2);
			fprintf(stdout, "Depth=%d\n", depth);
			fflush(stdout);*/
			if( layerkey2 > layerkey ) {
				for( tkey = layerkey; tkey <= layerkey2; tkey++ ) {
					if( pTrack->data.ptrs[tkey] )
						r->Push( pTrack->data.ptrs[ tkey ] );
				}
			} else {
				fprintf(stderr, "Keys in wrong order->\n");
				fflush(stderr);
				for( tkey = layerkey2; tkey <= layerkey; tkey++ ) {
					if( pTrack->data.ptrs[tkey] )
						r->Push( pTrack->data.ptrs[ tkey ] );
				}
			}

			while( (pTrack=(DeepHashMap64*)r->Iterate()) ) {
				//fprintf(stderr, "Scanning %d keys\n", r->stack->count);

				if( !pTrack->deeper ) {
					if( ( pTrack->key >= idc0 && pTrack->key <= idc1 ) ) {
						// either way we want it
						results->Append( pTrack->data.vals );
					} else {
						rejectCount++;
					}
				} else {
					branchCount++;
					for( tkey = 0; tkey < layermax; tkey++ ) {
						if( pTrack->data.ptrs[tkey] )
							r->Push( pTrack->data.ptrs[tkey] );
					}
					//fprintf(stderr, "Now scanning %d keys\n", r->stack->count);
					//fflush(stderr);
				}
			}

			//fprintf(stdout, "Rejected %lu records\n", rejectCount);
			//fprintf(stdout, "Explored %lu branches\n", branchCount);
			//fflush(stdout);

			deleteMem(r);
			return results;
		}

		pNext = pTrack->data.ptrs[ layerkey ];
		if( !pNext ) {
			fprintf(stdout, "GetRange: depth data lost.\n");
			break;
		}

		depth++;
	}

	deleteMem(r);
	return results;
}



void DHashmap64::Add( uint64_t idc, void *v )
{
	uint8_t depth, layern, oldlayern;
	uint64_t ratekey, oldratekey;
	DeepHashMap64 *y, *x = _Get(idc, &depth);

	if( !x ) {
		fprintf(stderr,"dhash is full?\n");
		throw "dhash shouldn't fill.";
	}

	if( !x->deeper ) {
		if( x->data.vals == NULL ) {
			x->data.vals = new tlist;
			x->key = idc;
		}
		if( x->key == idc ) {
			x->data.vals->PushBack(v);
			count++;
			return;
		}
	}

	//fprintf(stdout, "New list: %p %d\n", result->data.vals, result->data.vals->count);

	if( x->deeper ) {

		ratekey = LayerKey(idc, depth); // uint32_max, keymax )
/*		if( depth == 0 ) {
			ratekey = Keymod(idc);
		} else if( depth == 1 ) {
			ratekey = Keymod(idc, keymax, keyrates[0]);
		} else {
			ratekey = Keymod(idc, keyrates[depth-2], keyrates[depth-1]);
		}*/
		//ratekey = idc % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		//layern = floor( (float)ratekey / (float)keyrates[depth] );
		layern = ratekey;
		if( x->data.ptrs[layern] != NULL ) { // shouldn't happen because get_ should go all the way deep
			fprintf(stdout, "Losing a pointer!\n");
		}
//		fprintf(stdout, "Add[1]\n");
		//fflush(stdout);
		x->data.ptrs[ layern ] = new_deephashmap64(idc,v);
		count++;
		return;
	}

	y=x;
	do {
		if( depth > maxdepth ) {
			// just push the result into y. we're sure y is an edge node because deeper is implied == false.
			//fprintf(stdout, "Add[2] @ %p\n", y);
			if( y->deeper ) {
				fprintf(stdout, "WTF?!\n");
			}
			fflush(stdout);
			y->data.vals->PushBack( v );
			count++;
			return;
		}
		x = y;
		y = new_deephashmap64(); // x->key,x->data.vals);
		y->key = x->key;
		y->data.vals = x->data.vals;
		y->deeper = false;

		x->deeper = true;

		x->data.ptrs = (DeepHashMap64**)grabMem(sizeof(DeepHashMap64*)*layermax);
		memset( x->data.ptrs, 0, sizeof(DeepHashMap64*)*layermax );


		oldratekey = LayerKey(y->key, depth);
		/*
		if( depth == 0 ) {
			oldratekey = Keymod(y->key); // uint32_max, keymax )
		} else if( depth == 1 ) {
			oldratekey = Keymod(y->key, keymax, keyrates[0]);
		} else {
			oldratekey = Keymod(y->key, keyrates[depth-2], keyrates[depth-1]);
		}
		//oldratekey = y->key % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		oldlayern = floor( (float)oldratekey / (float)keyrates[depth] );
		*/
		oldlayern = oldratekey;

		x->data.ptrs[ oldlayern ] = y;

		ratekey = LayerKey(idc,depth);
		/*
		if( depth == 0 ) {
			ratekey = Keymod(idc); // uint32_max, keymax )
		} else if( depth == 1 ) {
			ratekey = Keymod(idc, keymax, keyrates[0]);
		} else {
			ratekey = Keymod(idc, keyrates[depth-2], keyrates[depth-1]);
		}
		//ratekey = idc % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		layern = floor( (float)ratekey / (float)keyrates[depth] );
		*/
		layern = ratekey;

		depth++;
	} while( layern == oldlayern );

	if( x->data.ptrs[layern] != NULL ) { // shouldn't happen because get_ should go all the way deep
		fprintf(stdout, "Losing a pointer2! depth=%d, max=%d\n", depth, maxdepth);
	}
	x->data.ptrs[ layern ] = new_deephashmap64(idc,v);
	count++;
	//fprintf(stdout, "Add[3]\n");
	//fflush(stdout);
}

bool DHashmap64::Has( uint64_t idc )
{
	uint8_t depth;
	DeepHashMap64 *x = _Get(idc,&depth);
	if( x && !x->deeper && x->key == idc ) return true;
	return false;
}

uint64_t DHashmap64::pointerkey( void *ptr )
{
	uintptr_t x = (uintptr_t)ptr;
	return x;
}

uint64_t DHashmap64::Keymod( uint64_t idc, uint64_t realmax, uint64_t mymax )
{
	if( mymax == 0 )
		mymax = keymax;
	if( !usedivmod )
		return idc % mymax;

	if( realmax == 0 )
		realmax = 0xffffFFFFffffFFFF; // that's 4*8=32 ones folks
	double approxValue = (double)idc/(double)realmax;
	if( approxValue > 1.0 ) {
		fprintf(stderr, "Error: key range (%f)\n", approxValue);
		fflush(stderr);
		abort();
	}
	uint64_t result = floor( approxValue * mymax );
//	fprintf(stdout, "Keymod(%llu|%llu) = %f->%llu (%llu)\n", idc, realmax, (float)approxValue, mymax, result);
//	fflush(stdout);
	return result;
}
// KeymodHigh: return the upper bound of the approximate number
uint64_t DHashmap64::KeymodHigh( uint64_t idc, uint64_t realmax, uint64_t mymax )
{
	return Keymod(idc,realmax,mymax);/*
	if( mymax == 0 )
		mymax = keymax;
	if( !usedivmod )
		return idc % mymax;

	if( realmax == 0 )
		realmax = 0xffffFFFFffffFFFF; // that's 4*8=32 ones folks
	float approxValue = (double)idc/(double)realmax;
	return (uint64_t) ceil( approxValue * mymax ); */
}

void DHashmap64::Del( uint64_t idc, void *v )
{
	uint8_t depth;
	uint64_t layerkey, lastkey, ckey = idc;
	DeepHashMap64 *i, *j;

	depth = 0;
	for( i = &top; i; i = j ) {
		if( !i->deeper ) {
			fprintf(stderr, "dhashmap::del couldn't go deeper\n");
			fflush(stderr);
			return;
		}
		if( depth > maxdepth ) {
			fprintf(stderr, "dhashmap::del maxdepth exception?\n");
			fflush(stderr);
			return;
		}

		if( depth <= 1 ) lastkey = Keymod(ckey, depth==0?0:keymax, depth==0?keymax:keyrates[0]);
		else lastkey = Keymod(ckey, keyrates[depth-2], keyrates[depth-1]);
		layerkey = floor( (float)lastkey / (depth<maxdepth?(float)keyrates[depth]:1) );
		ckey = ckey - layerkey * keyrates[depth-1];

/*
		layerkey = floor( (float)lastkey / (float)keyrates[depth] );
		ckey = ckey - layerkey * keyrates[depth];
		lastkey = Keymod( ckey, depth==0?keymax:keyrates[depth-1], keyrates[depth]);
		*/

		j = i->data.ptrs[ layerkey ];
		if( !j ) {
			fprintf(stderr, "dhashmap::del couldn't find subtree\n");
			return;
		}

		if( !j->deeper && j->key == idc ) {

			tnode *n;

			for( n = j->data.vals->nodes; n; n = n->next ) {
				if( n->data == v ) {
					j->data.vals->Pull( n );
					count = count - 1;
					//fprintf(stderr, "(delete node: count is %lu)\n", count);
					//fflush(stderr);
					break;
				}
			}

			if( j->data.vals->count <= 0 ) { // collapse the node
				i->data.ptrs[layerkey] = NULL;
				free_deephashmap64(j, layermax);
			}

			return;
		}

		depth++;
	}

	throw "invalid code marker 2387493";
}
