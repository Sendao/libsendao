#include "sendao.h"

/*
int charkey( const char c )
{
	int a;
	if(( c >= 'a' && c <= 'z' ) || ( c >= 'A' && c <= 'Z' )){
		a = 1 + (LOWER(c) - 'a');
	} else if((c >= '0') && (c <= '9')) {
		a = 27 + (c - '0');
	} else {
		a = 38;
	}
	return a;
}
*/



HTable::HTable( uint32_t kmax )
{
	tab = (tlist**)grabMem( kmax * sizeof(tlist*) );
	for( keymax = 0; keymax < kmax; keymax ++ ) {
		tab[keymax] = NULL;
	}
	usedkeys = 0;
}
HTable::~HTable()
{
	Clear();
	releaseMem(tab);
}

void HTable::Clear( voidFunction *vF )
{
	uint32_t i;

	for( i = 0; i < keymax; i++ ) {
		if( tab[i] ) {
			tab[i]->Clear(vF);
			deleteMem(tab[i]);

			tab[i] = NULL;
		}
	}
	usedkeys = 0;
}
void HTable::ClearWith( void2Function *vF, sint32_t data )
{
	uint32_t i;

	for( i = 0; i < keymax; i++ ) {
		if( tab[i] ) {
			tab[i]->ClearWith(vF, data);
			deleteMem(tab[i]);

			tab[i] = NULL;
		}
	}
	usedkeys = 0;
}

void HTable::CopyFrom( HTable *x, keyFunction *vK, cloneFunction *vC )
{
	tnode *n;
	void *p, *px;
	uint32_t i, j;
	tlist *y;

	for( i = 0; i < x->keymax; i++ ) {
		if( !x->tab[i] ) continue;
		y = x->tab[i];
		forTLIST( p, n, y, void* ) {
			px = vC(p);
			j = vK(px);
			Add(j,px);
		}
	}
}

void HTable::CopyFrom( tlist *x, keyFunction *vK, cloneFunction *vC )
{
	tnode *n;
	void *p, *px;
	uint32_t i;

	forTLIST( p, n, x, void* ) {
		px = vC(p);
		i = vK(px);
		Add(i,px);
	}
}

tlist *HTable::Slice( uint32_t i0, uint32_t imax, cloneFunction *vC, bool pointerClone )
{
	tlist *nx = new tlist;
	tnode *n;
	void *p;
	void *px;
	uint32_t i, j=0, k=0;

	for( i = 0; i < keymax; i++ ) {
		if( tab[i] ) {
			for( n = tab[i]->nodes; n; n = n->next ) {
				p = n->data;

				j++;
				if( j < i0 )
					continue;
				k++;
				if( imax != 0 && k >= imax )
					continue;

				if( vC ) {
					nx->PushBack( vC(n->data) );
				} else if( pointerClone ) {
					px = grabMem(sizeof(void*)); // basically we just dereference the pointer into a memory slot, so if it's an int, the int goes there. blah boring
					memcpy(px, &p, sizeof(void*));
					nx->PushBack(px);
				} else {
					nx->PushBack( n->data );
				}
			}
		}
	}

	return nx;
}

tlist *HTable::ToList( cloneFunction *vC, bool pointerClone )
{
	tlist *nx = new tlist;
	tnode *n;
	void *p;
	void *px;
	uint32_t i;

	for( i = 0; i < keymax; i++ ) {
		if( tab[i] ) {
			for( n = tab[i]->nodes; n; n = n->next ) {
				p = n->data;
				if( vC ) {
					nx->PushBack( vC(n->data) );
				} else if( pointerClone ) {
					px = grabMem(sizeof(void*)); // basically we just dereference the pointer into a memory slot, so if it's an int, the int goes there. blah boring
					memcpy(px, &p, sizeof(void*));
					nx->PushBack(px);
				} else {
					nx->PushBack( n->data );
				}
			}
		}
	}

	return nx;
}


tlist *HTable::Search( uint32_t key )
{
	uint32_t ki = key % keymax;
	return tab[ki];
}
void HTable::Foreach( void cb(void *) )
{
	uint32_t i;
	tnode *n, *next;

	for( i=0; i<keymax; i++ ) {
		if( !tab[i] ) continue;
		for( n = tab[i]->nodes; n; n = next ) {
			next = n->next;
			cb( n->data );
		}
	}
}
void HTable::Foreach( uint32_t i, void cb(void *) )
{
	tnode *n, *next;

	if( !tab[i] ) return;
	for( n = tab[i]->nodes; n; n = next ) {
		next = n->next;
		cb( n->data );
	}
}
void HTable::SetRange( uint32_t kmax )
{
	if( usedkeys != 0 ) {
		printf("SetRange on a non-empty HTable");
		Clear();
	}
	if(tab) releaseMem(tab);
	tab = (tlist**)grabMem( kmax * sizeof(tlist*) );
	for( keymax = 0; keymax < kmax; keymax ++ ) {
		tab[keymax] = NULL;
	}
	usedkeys = 0;
}
bool HTable::Remove( uint32_t key, void *ptr )
{
	uint32_t ki = key % keymax;
	tlist *l = tab[ki];
	if( !l ) return false;
	tnode *n = l->Pull(ptr);
	if( !n ) return false;
	deleteMem(n);

	if( l->count == 0 ) {
		deleteMem(tab[ki]);

		tab[ki]=NULL;
		usedkeys--;
	}
	return true;
}
bool HTable::Has( uint32_t key, void *ptr )
{
	uint32_t ki = key % keymax;
	tlist *l = tab[ki];
	tnode *n;

	for( n = l ? l->nodes : NULL; n; n = n->next )
		if( n->data == ptr ) return true;

	return false;
}
void HTable::Add( uint32_t key, void *ptr )
{
	uint32_t ki = key % keymax;
	if( !tab[ki] ) {
		tab[ki] = new tlist();
		usedkeys++;
	}
	tab[ki]->PushBack( ptr );
}
void HTable::AddList( uint32_t key, tlist *items )
{
	uint32_t ki = key % keymax;
	if( !tab[ki] ) {
		tab[ki] = new tlist();
		usedkeys++;
	}
	tab[ki]->Append(items);
}

/*

STable::STable( uint32_t kmax ) : HTable(kmax)
{
}
STable::~STable()
{
	Clear();
	releaseMem(tab); tab=NULL;
}

void STable::Clear()
{
	HTable::Clear();
}

void *STable::Get( const char *name )
{
	uint32_t key = NList::strkey(name), shortkey = (key % keymax);
	tlist *l = tab[shortkey];
	tnode *n;
	nvoid *nv;

	forTLIST( nv, n, l, nvoid* )
	{
		if( str_c_cmp(nv->name,name) == 0 ) {
			return nv->ptr;
		}
	}
	return NULL;
}
void STable::Set( const char *name, void *ptr )
{
	uint32_t key = NList::strkey(name), shortkey = (key % keymax);

	tlist *l = tab[shortkey];
	if( !l ) {
		l = tab[shortkey] = new tlist();
		usedkeys++;
	}

	nvoid *nv;
	tnode *n;

	forTLIST( nv, n, l, nvoid* )
	{
		if( str_c_cmp(nv->name, name) == 0 ) {
			nv->ptr = ptr;
			return;
		}
	}
	nv = (nvoid*)grabMem(sizeof(nvoid));
	nv->name = str_dup(name);
	nv->ptr = ptr;
	l->PushBack( nv );
}


*/
