#include "sendao.h"
#include <errno.h>


bool check_file_exists( const char *fileName )
{
	FILE *fp = fopen(fileName, "r");
	if( !fp ) return false;
	fclose(fp);
	return true;
}


char *readFile( const char *fileName )
{
	stringbuf sb;
	char buf[1024];

	FILE *fp = fopen(fileName, "r");
	if( !fp ) {
		printf("Failed to open %s for read (Error: %s(%d))\n", fileName, strerror(errno), errno);
		return NULL;
	}
	while( fgets( buf, 1024, fp ) ) {
		sb.append(buf);
		sb.append("\n");
	}
	fclose(fp);
	if( !sb.p ) return NULL;

	return str_dup(sb.p);
}


uint32_t charkey( char c )
{
	uint32_t a;
	if( c >= 'a' && c <= 'z' ) {
		a = 1 + (c - 'a');
	} else if( c >= 'A' && c <= 'Z' ){
		a = 1 + (c - 'A');
	} else if((c >= '0') && (c <= '9')) {
		a = 28 + (c - '0');
	} else {
		a = 38;
	}
	return a;
}



int strkey( const char *str )
{
	uint32_t v=0, j, c;

	for( j=0; str[j] && j < 6; j++ ) {
		c=charkey(str[j]);
		v = v*20 + c;
	}
	return v;
}

// Stolen from /usr/src/linux/lib/crc32.c
// Note: it's stolen because this is kernel code, but it'll work fine in GOD.
#define CRCPOLY_LE 0xedb88320
u32 crc32(const char *pin)
{
	int i;
	u32 crc=0;
	size_t len = strlen(pin);
	unsigned const char *p = (unsigned const char *)pin;

	while (len--) {
		crc ^= *p++;
		for (i = 0; i < 8; i++)
			crc = (crc >> 1) ^ ((crc & 1) ? CRCPOLY_LE : 0);
	}
	return crc;
}


int number_range(int low, int high)
{
    if( random_initted == false ) {
        random_initted=true;
#ifdef WIN32
        srand(clock());
#endif
#ifdef LINUX
        srandom(time(NULL));
#endif
    }
#ifdef WIN32
    return rand() % ((high+1)-low) + low;
#endif
#ifdef LINUX
    return random() % ((high+1)-low) + low;
#endif
}



int rrand(int low, int high)
{
	int val = low>=high?low : (low+(int)((float)(high-low)*rand()/(RAND_MAX+1.0)));

	return val;
}

float frand(float low, float high)
{
	float val = low>=high?low : low + (high-low) * rand() / (RAND_MAX+1.0);

	return val;
}

char *print_time( time_t st )
{
	struct tm *lt = localtime(&st);
	char buf[40];
	strftime(buf,40,"%c", lt);
	return str_dup(buf);
}


#include <cxxabi.h>


#ifndef WIN32

#include <execinfo.h>

#else

#include <dbghelp.h>

char **backtrace_symbols( void **bt, int maxlen )
{
	int n;
	DWORD64 diss;
	HANDLE hproc = GetCurrentProcess();
	struct _SYMBOL_INFO *sym;
	char **results = (char**)malloc( sizeof(char*) * maxlen );
	sym = (PSYMBOL_INFO)malloc(sizeof(*sym)+128);
	for( n=0; n<maxlen; ++n ) {
		memset( sym, 0, sizeof(*sym) + 127 );
		sym->SizeOfStruct = sizeof(*sym);
		sym->MaxNameLen = 127;
		SymFromAddr( hproc, (DWORD64)bt[n], &diss, sym );
		//IMAGEHLP_LINE64 line;
		//line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
		//SymGetLineFromAddr64( hproc, bt[n], &diss, &line );
		results[n] = str_dup( sym->Name );
	}
	return results;
}

int backtrace( void **bt, int maxlen )
//http://theorangeduck.com/page/printing-stack-trace-mingw
{
	int count=0;
	HANDLE hproc = GetCurrentProcess();
	HANDLE hthread = GetCurrentThread();
	CONTEXT context = {};
	context.ContextFlags = CONTEXT_FULL;
	RtlCaptureContext(&context);

	SymInitialize(hproc, NULL, TRUE);
	SymSetOptions(SYMOPT_LOAD_LINES);

	STACKFRAME64 frame = {};
	frame.AddrPC.Offset    = context.Rip;
	frame.AddrPC.Mode      = AddrModeFlat;
	frame.AddrFrame.Offset = context.Rsp;
	frame.AddrFrame.Mode   = AddrModeFlat;
	frame.AddrStack.Offset = context.Rsp;
	frame.AddrStack.Mode   = AddrModeFlat;

	while (StackWalk64(IMAGE_FILE_MACHINE_AMD64, hproc, hthread, &frame, &context , NULL, SymFunctionTableAccess64, SymGetModuleBase64, NULL))
	{
		// extract data
		if( count >= maxlen-1 ) break;
		bt[count] = (void*)frame.AddrPC.Offset;
		count++;
	}

	return count;
}
#endif

char *printStackTrace(void **bt, int maxlen)
{
	char **tracers;
	char *buf, *px, *px2, demanglable[80];
	int i, status;
	size_t funcsize=80;
	char *funcname = (char*)malloc(funcsize);
	stringbuf sb;

	tracers = backtrace_symbols(bt, maxlen);

	for( i=0; i<maxlen; i++ ) {
		px = strstr(tracers[i], "(");
		if( !px ) continue;
		px++;
		if( !(px2=strstr(tracers[i],"+")) )
			px2 = strstr(tracers[i], ")");
		memcpy( demanglable, px, px2-px );
		demanglable[px2-px] = '\0';
		buf = abi::__cxa_demangle(tracers[i], funcname, &funcsize, &status);
		if( status == 0 ) {
			funcname=buf;
			sb.printf("%s\n", buf);
		} else {
			sb.printf("%s\n", demanglable);
		}
		#ifdef WIN32
		releaseMem(tracers[i]);
		#endif
	}
	free(tracers);

	free(funcname);

	if( !sb.p ) return str_dup("");
	else return str_dup(sb.p);
}
