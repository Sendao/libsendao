#include "sendao.h"

DMap64::DMap64( uint64_t gran ) : HTable64(gran)
{
}

DMap64::~DMap64()
{
	Clear();// (voidFunction*)free_ivoid64 );
}

void DMap64::CopyFrom( HTable64 *src, keyFunction64 *vK, cloneFunction *vC )
{
	if( vK == NULL ) vK = keyof_ivoid64;
	if( vC == NULL ) vC = clone_ivoid64;
	HTable64::CopyFrom( src, vK, vC );
}
void DMap64::CopyFrom( tlist *src, keyFunction64 *vK, cloneFunction *vC )
{
	if( vK == NULL ) vK = keyof_ivoid64;
	if( vC == NULL ) vC = clone_ivoid64;
	HTable64::CopyFrom( src, vK, vC );
}

tlist *DMap64::ToList( cloneFunction *vC, bool copyNode, bool includeNode )
{
	tlist *nx = new tlist;
	tnode *n;
	ivoid64 *p, *p2;
	void *px;
	uint64_t i;

	for( i = 0; i < keymax; i++ ) {
		if( tab[i] ) {
			for( n = tab[i]->nodes; n; n = n->next ) {
				p = (ivoid64*)n->data;
				if( vC ) {
					if( includeNode || copyNode ) {
						nx->PushBack( vC( p ) );
					} else if( !p->ptr ) {
						continue;
					} else {
						px = vC(p->ptr);
						p2 = new_ivoid64(p->idc, px);
						nx->PushBack( (void*)p2 );
					}
				} else if( copyNode ) {
					nx->PushBack( p ) ;
				} else if( includeNode ) {
					nx->PushBack( clone_ivoid64( p ) );
				} else {
					nx->PushBack( p->ptr );
				}
			}
		}
	}

	return nx;
}


void DMap64::Clear( voidFunction *cb )
{
	if( cb ) Foreach(cb);
	HTable64::Clear((voidFunction*)free_ivoid64);
}

void DMap64::Foreach( voidFunction *cb )
{
	uint64_t i;
	tnode *n, *next;
	ivoid64 *iv;

	for( i=0; i<keymax; i++ ) {
		if( !tab[i] ) continue;
		for( n = tab[i]->nodes; n; n = next ) {
			next = n->next;
			iv = (ivoid64*)n->data;
			cb( iv->ptr );
		}
	}
}

ivoid64 *DMap64::Get_( uint64_t idc )
{
	tlist *l = HTable64::Search(idc);
	tnode *n;
	ivoid64 *iv;

	forTLIST( iv, n, l, ivoid64* ) {
		if( iv->idc == idc ) {
			return iv;
		}
	}

	return NULL;
}
void *DMap64::Get( uint64_t idc )
{
	ivoid64 *x = Get_(idc);
	return x ? x->ptr : NULL;
}

void DMap64::Set( uint64_t idc, void *v )
{
	ivoid64 *iv = new_ivoid64(idc,v);
	HTable64::Add(idc,iv);
}

bool DMap64::Has( uint64_t idc )
{
	tlist *l = HTable64::Search(idc);
	tnode *n;
	ivoid64 *iv;

	forTLIST( iv, n, l, ivoid64* ) {
		if( iv->idc == idc ) {
			return true;
		}
	}
	return false;
}

void DMap64::Del( uint64_t idc )
{
	tlist *l = HTable64::Search(idc);
	tnode *n, *nn;
	ivoid64 *iv;

	forTSLIST( iv, n, l, ivoid64*, nn ) {
		if( iv->idc == idc ) {
			l->Pull(n);
			free_ivoid64(iv);
		}
	}

	if( l && l->count == 0 ) {
		deleteMem(l);

		tab[idc%keymax]=NULL;
		usedkeys--;
	}
}


/*

SMap::SMap( uint64_t gran ) : HTable64(gran)
{
}

SMap::~SMap()
{
	Clear( (voidFunction*)free_nkvoid );
}

void SMap::CopyFrom( HTable *src, keyFunction *vK, cloneFunction *vC )
{
	if( vK == NULL ) vK = keyof_nkvoid;
	if( vC == NULL ) vC = clone_nkvoid;
	HTable64::CopyFrom( src, vK, vC );
}
void SMap::CopyFrom( tlist *src, keyFunction *vK, cloneFunction *vC )
{
	if( vK == NULL ) vK = keyof_nkvoid;
	if( vC == NULL ) vC = clone_nkvoid;
	HTable64::CopyFrom( src, vK, vC );
}

tlist *SMap::ToList( cloneFunction *vC, bool includeNode )
{
	tlist *nx = new tlist;
	tnode *n;
	nkvoid *p, *p2;
	void *px;
	uint64_t i;

	for( i = 0; i < keymax; i++ ) {
		if( tab[i] ) {
			for( n = tab[i]->nodes; n; n = n->next ) {
				p = (nkvoid*)n->data;
				if( vC ) {
					px = vC(p->ptr);
					p2 = new_nkvoid(p->name, p->idc, px);
					nx->PushBack( (void*)p2 );
				} else if( includeNode ) {
					nx->PushBack( clone_nkvoid( p ) );
				} else {
					nx->PushBack( p->ptr );
				}
			}
		}
	}

	return nx;
}

nkvoid *SMap::Get_( uint64_t idc )
{
	tlist *l = HTable64::Search(idc);
	tnode *n;
	nkvoid *iv;

	forTLIST( iv, n, l, nkvoid* ) {
		if( iv->idc == idc ) {
			return iv;
		}
	}

	return NULL;
}
void *SMap::Get( uint64_t idc )
{
	nkvoid *x = Get_(idc);
	return x ? x->ptr : NULL;
}

nkvoid *SMap::Get_( const char *s )
{
	uint64_t idc = namekey(s);
	tlist *l = HTable64::Search(idc);
	tnode *n;
	nkvoid *iv;

	forTLIST( iv, n, l, nkvoid* ) {
		if( iv->idc == idc && str_c_cmp( iv->name, s ) == 0 ) {
			return iv;
		}
	}

	return NULL;
}
void *SMap::Get( const char *s )
{
	nkvoid *x = Get_(s);
	return x ? x->ptr : NULL;
}

void SMap::Set( const char *s, void *v )
{
	uint64_t idc = namekey(s);
	nkvoid *iv = new_nkvoid(s,idc,v);
	HTable64::Add(idc,iv);
}

void SMap::Foreach( voidFunction *cb )
{
	uint64_t i;
	tnode *n, *next;
	nkvoid *iv;

	for( i=0; i<keymax; i++ ) {
		if( !tab[i] ) continue;
		for( n = tab[i]->nodes; n; n = next ) {
			next = n->next;
			iv = (nkvoid*)n->data;
			cb( iv->ptr );
		}
	}
}

void SMap::Del( uint64_t idc )
{
	tlist *l = HTable64::Search(idc);
	tnode *n, *nn;
	nkvoid *iv;

	forTSLIST( iv, n, l, nkvoid*, nn ) {
		if( iv->idc == idc ) {
			l->Pull(n);
			free_nkvoid(iv);
		}
	}
}

void SMap::Del( const char *s )
{
	uint64_t idc = namekey(s);
	tlist *l = HTable64::Search(idc);
	tnode *n, *nn;
	nkvoid *iv;

	forTSLIST( iv, n, l, nkvoid*, nn ) {
		if( iv->idc == idc && str_c_cmp( iv->name, s ) == 0 ) {
			l->Pull(n);
			free_nkvoid(iv);
		}
	}
}

void SMap::Del( nkvoid *ptr )
{
	tlist *l = HTable64::Search( ptr->idc );
	tnode *n, *nn;
	nkvoid *iv;

	forTSLIST( iv, n, l, nkvoid*, nn ) {
		if( iv == ptr ) {
			l->Pull(n);
			free_nkvoid(iv);
		}
	}
}
*/
