#include "sendao.h"

#define MEM_PAD						8

// Todo: all new pointerbuffers should go into a control list

quick_list *pointerbuffers = NULL;

int MEM_UNITS=0;
PointerBuffer **mainMemory = NULL;
bool mainMem=false;
bool use_membug=false;
bool first_report=true;
bool defragging=false;
uint64_t totalMemory=0;
timestop_v Squicklists;
bool prepped_quicklists=false;

quick_list *prememset=NULL;
void unquicklist( quick_list *ql );
void releaseQuickLists(void);
int prep_slots = 0;


void prepMemory( int slotSize, int slotChunks, bool doTrace )
{
	uint32_t *preparH = (uint32_t*)malloc( sizeof(uint32_t) * 3 );
	preparH[0] = slotSize;
	preparH[1] = slotChunks;
	preparH[2] = doTrace?1:0;
	if( !prepped_quicklists ) {
		// we are pre-init
		prepped_quicklists=true;
		tsInit( &Squicklists );
	}
	prememset = quicklist( (void*)preparH, prememset );
	prep_slots++;
}

void exitMemory( void )
{
	PointerBuffer *pb;
	quick_list *ql, *qln;
	fprintf(stdout, "Exit memory...\n");
	fflush(stdout);
	for( ql = pointerbuffers; ql; ql = qln ) {
		qln = ql->next;
		pb = (PointerBuffer*)ql->ptr;
		deleteMem( pb );
		unquicklist(ql);
	}
	pointerbuffers = NULL;

	free( mainMemory );
	mainMemory=NULL;
	releaseQuickLists();
}

void initMemory( int numSlots, int *in_sizes, int *in_chunks, bool *in_trace )
{
	int i;
	PointerBuffer *pb;
	uint32_t *sizes=NULL;//= {     1,	16,		64, 512, 4096, 32768, 262144, 2097152 };
	uint32_t *zchunks=NULL;// = { 2048, 1024, 512, 24, 	24, 	32, 	16, 		4 };
	bool *trace=NULL;// = { false, false, true, false, false, false, false, false };

	if( !prepped_quicklists )
		tsInit( &Squicklists );
	if( prep_slots != 0 )
		numSlots = prep_slots;
	MEM_UNITS = numSlots;
	fprintf(stdout, "Chunks found: %d\n", numSlots);
	fflush(stdout);
	trace = (bool*)malloc( sizeof(bool)*numSlots );
	sizes = (uint32_t*)malloc( sizeof(uint32_t)*numSlots );
	zchunks = (uint32_t*)malloc( sizeof(uint32_t)*numSlots );

	if( prememset ) {
		quick_list *x, *xn;
		uint32_t *preparH;
		i=MEM_UNITS-1;
		for( x = prememset; x; x = xn ) {
			if( i < 0 ) {
				fprintf(stderr, "shouldn't be doing that\n");
				break;
			}
			xn = x->next;
			preparH = (uint32_t*)x->ptr;
			sizes[i] = preparH[0];
			zchunks[i] = preparH[1];
			trace[i] = preparH[2] == 1 ? true : false;
			fprintf(stdout, "prepared memory size: %u chunks: %u trace? %s\n", sizes[i], zchunks[i], trace[i]?"true":"false");
			fflush(stdout);
			unquicklist(x);
			free(preparH);
			i--;
		}
		prememset=NULL; // releasing dead pointers is recommended for debugging and 3rd party tracking purposes
	} else if( in_sizes ) {
		for( i=0; i<MEM_UNITS; ++i ) {
			trace[i] = in_trace ? in_trace[i] : false;
			zchunks[i] = in_chunks[i];
			sizes[i] = in_sizes[i];
		}
	} else { // defaults
		for( i=0; i<MEM_UNITS; ++i )
			trace[i] = false;

		sizes[0] = 1;				zchunks[0] = 2048;
		sizes[1] = 16;			zchunks[1] = 1024;
		sizes[2] = 64;			zchunks[2] = 512;
		sizes[3] = 512;			zchunks[3] = 24;
		sizes[4] = 4096;		zchunks[4] = 24;
		sizes[5] = 32768;		zchunks[5] = 32;
		sizes[6] = 262144;	zchunks[6] = 16;
		sizes[7] = 2097152;	zchunks[7] = 4;
	}

	mainMemory = (PointerBuffer**)malloc( sizeof(PointerBuffer*) * (unsigned)numSlots );

	for( i=0; i<numSlots; ++i ) {
		mainMemory[i]=pb=new PointerBuffer( sizes[i], trace[i] );
		pb->chunk = zchunks[i];
		fprintf(stdout, "Chunk %d size: %u x %u\n", i, sizes[i], zchunks[i]);
		pb->_Alloc();
	}

	fprintf(stdout, "Main memory initialized.\n");
	fflush(stdout);

	fprintf(stdout, "Querulous pointers %p, %p, %p\n", zchunks, sizes, trace);
	free( zchunks );
	free( sizes );
	free( trace );

	mainMem=true;
	totalMemory=0;
	use_membug=false;
}



quick_list *qlptrs=NULL, *qlareas=NULL;
void *qlarea=NULL;
uint32_t qlsize=0, qlused=0;

void releaseQuickLists(void)
{
	quick_list *ql;
	while( qlptrs ) { // we have stored pointers
		free( qlptrs->ptr );
		ql = qlptrs;
		qlptrs = qlptrs->next;
		free(ql);
	}
	while( qlareas ) {
		free( qlareas->ptr );
		ql = qlareas;
		qlareas = qlareas->next;
		free(ql);
	}

	if( qlarea ) {
		free(qlarea);
		qlarea=NULL;
	}
}

quick_list *quicklist( void *ptr, quick_list *ql )
{
	quick_list *n;


	tsEnter( &Squicklists );

	if( qlptrs ) { // we have stored pointers
		void *qlzone = qlptrs->ptr;
		//fprintf(stdout, "qlzone=%p\n", qlzone);
		unsigned char *qlcount = (unsigned char*)qlzone;
		*qlcount = *qlcount - 1;
		quick_list **qlv = (quick_list**)( (char*)qlzone + sizeof(char) + sizeof(quick_list*) * (*qlcount) );
		n = *qlv;//[*qlcount];
//		fprintf(stdout, "qlc0unt=%d @ %p => %p\n", (char)*qlcount, qlv, n);
		if( *qlcount == 0 ) { // release the stored pointers
			free( qlzone );
			quick_list *qltemp = qlptrs; //! todo: make the recursion work instead and unquicklist()
			qlptrs = qlptrs->next;
			free(qltemp);
		}
	} else {
		if( !qlarea || qlused+sizeof(quick_list) > qlsize ) {
			if( qlarea ) {
				n = (quick_list*)malloc( sizeof(quick_list) );
				n->ptr = qlarea;
				n->next = qlareas;
				n->prev = NULL;
				qlareas = n;
			}
			qlsize = 1024*100 * sizeof(quick_list);
			qlarea = malloc( qlsize );
			qlused = 0;
		}
		n = (quick_list*)( (char*)qlarea + qlused );
		qlused += sizeof(quick_list);
	}

	n->next = ql;
	n->prev = NULL;
	n->ptr = ptr;
	if( ql )
		ql->prev = n;


	tsExit( &Squicklists );

	return n;
}

void unquicklist( quick_list *ql )
{
	void *qlzone;
	unsigned char *qlcount;
	quick_list **qlv, *qlz;

	if( !ql ) return;

	tsEnter( &Squicklists );
	if( ql->next ) ql->next->prev = ql->prev;
	if( ql->prev ) ql->prev->next = ql->next;

	if( !qlptrs ) { // make the first unquicklist:
		qlz = (quick_list*)malloc(sizeof(quick_list));
		qlz->next = qlptrs;
		qlz->ptr = qlzone = malloc( sizeof(quick_list*) * 252 + sizeof(char) );
		qlz->prev = NULL;
		qlptrs = qlz;
		qlcount = (unsigned char*)qlzone;
		*qlcount = 0;
		qlv = (quick_list**)(  (char*)qlzone + sizeof(char)  );
	} else {
		qlzone = qlptrs->ptr;
		qlcount = (unsigned char*)qlzone;
		//fprintf(stdout, "qlcount=%d (%p)\n", (int)*qlcount, qlcount);
		qlv = (quick_list**)(  (char*)qlzone + sizeof(char) + sizeof(quick_list*) * (*qlcount)  );
	}
	if( qlcount[0] == 250 ) {
		qlz = (quick_list*)malloc(sizeof(quick_list));
		qlz->next = qlptrs;
		qlz->ptr = qlzone = malloc( sizeof(quick_list*) * 252 + sizeof(char) );
		qlz->prev = NULL;
		qlptrs = qlz;
		qlcount = (unsigned char*)qlzone;
		*qlcount = 0;
		qlv = (quick_list**)(  (char*)qlzone + sizeof(char) );
	}

	*qlv = ql;
	*qlcount = *qlcount + 1;

	tsExit( &Squicklists );
}


void *operator new(std::size_t s)
{
	void *p = grabMem(s);
	#ifdef USE_PTRMAP
	GP->Save(p,s);
	#endif
	return p;
}
/*
void operator delete(void *p) throw()
{
	fprintf(stdout,"delete()\n");
	fflush(stdout);
	if( !p ) return;
	#ifdef USE_PTRMAP
	GP->Del(p);
	#else
	releaseMem(p);
	#endif
}
*/
void operator delete(void *p) throw()
{
	if( !p ) return;
	#ifdef USE_PTRMAP
	GP->Del(p);
	#else
	releaseMem(p);
	#endif
}
void *operator new[](std::size_t s)
{
	void *p = grabMem(s);
	#ifdef USE_PTRMAP
	GP->Save(p,s);
	#endif
	return p;
}

void operator delete[](void *p) throw()
{
	if( !p ) return;
	#ifdef USE_PTRMAP
	GP->Del(p);
	#else
	releaseMem(p);
	#endif
}

char *reportMem( void )
{
//	int i;
	stringbuf sb;
	char *tStr;

	PointerBuffer *pb;
	quick_list *ql;

	for( ql = pointerbuffers; ql; ql = ql->next ) {
		pb = (PointerBuffer*)ql->ptr;

		tStr = reportMem( pb );
		sb.printf( "%s\n", tStr );
		releaseMem(tStr);
	}

	char *results = str_dup(sb.p);
	return results;
}
char *reportMem( PointerBuffer *pb )
{
	stringbuf sb;
	quick_list *ql;
	int cGroups=0, cSingles=0, cChunks=0;
	uint64_t groups_freed=0, singles_freed=0;
	size_t midlen0 = 40, midlen1 = 60, midlen2 = 80;
	mem_track *mt;
	mem_trace *me, *men;
	char *stktrc;
	time_t reporting_time = time(NULL) - 300; // 5 minutes

	// count they quicklists and add they numbers
	for( ql = pb->freeSingles; ql; ql = ql->next )
		cSingles++;
	for( ql = pb->chunks; ql; ql = ql->next ) {
		if( pb->tracing ) {
			for( me = (mem_trace*)ql->ptr; me; me = men ) {
				if( (char*)me >= (char*)ql->ptr + pb->unit*pb->chunk )
					break;
				if( ql == pb->chunks && (char*)me >= (char*)ql->ptr + pb->chunkused )
					break;
				men = (mem_trace*)( (char*)me + me->sz );
				if( me->sz == 0 ) {
					//sb.printf("zero-size block\n");
					break;
				}
				if( me->refs <= 0 ) continue; // already free'd
				if( first_report && me->alloccount != 0 ) {
					free(me->alloctrace);
					me->alloccount = 0;
					continue;
				}

				if( me->alloccount != 0 && me->allocTime < reporting_time ) {
					stktrc = printStackTrace( me->alloctrace, me->alloccount );
					sb.printf("memory from:\n%s\n", stktrc);
					releaseMem(stktrc);
					free(me->alloctrace);
					me->alloccount = 0;
				}
			}
		}
		cChunks++;
	}
	singles_freed = pb->unit*cSingles;

	if( first_report )
		first_report = false;

	for( ql = pb->freeGroups; ql; ql = ql->next ) {
		if( pb->tracing ) {
			me = (mem_trace*)ql->ptr;
			cGroups++;
			groups_freed += me->sz;
		} else {
			mt = (mem_track*)ql->ptr;
			cGroups++;
			groups_freed += mt->sz;
		}
	}
	sb.printf("%d %d-byte blocks, %d used ", pb->chunk, pb->unit, cChunks);
	if( strlen(sb.p) < midlen0 )
		sb.append( ' ', midlen0-strlen(sb.p) );
	sb.printf(" singles: %d/%lu ", cSingles, singles_freed);
	if( strlen(sb.p) < midlen1 )
		sb.append( ' ', midlen1-strlen(sb.p) );
	sb.printf("groups: %d/%lu ", cGroups, groups_freed);
	if( strlen(sb.p) < midlen2 )
		sb.append( ' ', midlen2-strlen(sb.p) );
	sb.printf("allocs: %lu", pb->allocations);
	pb->allocations=0;

	char *results = str_dup(sb.p);
	return results;
}

void *do_defragMem( void *nptr );

void defragMem( void )
{
	if( defragging ) return;
	defragging = true;
	tsSplit( do_defragMem, NULL );
	//do_defragMem(NULL);
}

// defragMem: move free groups
void *do_defragMem( void *nptr )
{
/*
quick_list *ql, *qln, *qlp, *qlT;
stringbuf sb;
PointerBuffer *pbp;
mem_track *mt;
*/
	int total_moved=0;//, i;
	PointerBuffer *pb;
	quick_list *ql;

	for( ql = pointerbuffers; ql; ql = ql->next ) {
		pb = (PointerBuffer*)ql->ptr;
	//for( i=MEM_UNITS-1; i>0; --i ) {
		//pb = mainMemory[i];
		pb->Defrag();
		continue;
		/*
		pbp = mainMemory[i-1];
		for( ql = pb->freeGroups; ql; ql = qln ) {
			qln = ql->next;
			mt = (mem_track*)ql->ptr;

			if( mt->sz > pbp->unit ) {
				total_moved++;
				if( ql->prev )
					ql->prev->next = ql->next;
				if( ql->next )
					ql->next->prev = ql->prev;

				ql->prev = NULL;
				if( pbp->freeGroups )
					pbp->freeGroups->prev = ql;
				ql->next = pbp->freeGroups;
				pbp->freeGroups = ql;
			}
		}
		*/
	}
	/*
	fprintf(stdout, "Defragging completed\n");
	fflush(stdout);
	if( total_moved != 0 ) {
		fprintf(stdout, "Processed %d defragmentation records\n", total_moved);
		fflush(stdout);
	}*/
	defragging = false;

	return NULL;
}
void *grabMem( size_t sz )
{
	PointerBuffer *pb;
	void *ptr;
	int i;

	//fprintf(stdout, "Request %ld\n", sz);
	sz = sz + sizeof(uint32_t) + MEM_PAD;

	if( mainMem ) {
		for( i=0; i<MEM_UNITS-1; ++i ) {
			if( sz <= mainMemory[i]->pubunit )
				break;
		}
		//fprintf(stdout, "grab %d, pb=%d\n", sz, i);
		pb = mainMemory[i];
		if( sz <= pb->pubunit )
			ptr = pb->Create();
		else
			ptr = pb->CreateActualSize(sz);
		i = i + 1;
	} else {
		ptr = malloc( sz );
		i = 0x9999;
	}
	memset(ptr,0,sz);
	uint32_t *px = (uint32_t*)ptr;
	*px = (uint32_t)i;
	void *ptrOut = (char*)ptr + sizeof(uint32_t) + MEM_PAD;
	return ptrOut;
}
void releaseMem( void *p )
{
	if( !p ) return;

	uint32_t *px = (uint32_t*)( (char*)p - (MEM_PAD + sizeof(uint32_t)) );

	if( use_membug ) {
		fprintf(stdout, "release %p, pb=%d\n", p, (*px)-1);
		fflush(stdout);
	}
	if( *px == 0x9999 ) {
		//fprintf(stdout, "Release main memory\n");
		fflush(stdout);
		free(px);
	} else if( *px < (unsigned)(MEM_UNITS+1) ) {
		mainMemory[(*px)-1]->Release( (void*)px );
	} else {
		fprintf(stdout, "releaseMem: nope\n");
		fflush(stdout);
	}
}


/*
sint8_t memfindBrancher( Tree *tree, TreeBranch *branch, void *ptr )
{
	float rp = 127.0 * ( MEM_RANGE / ( ptr - MEM_START ) )
	return DHashmap::pointerkey(ptr);
}
*/
/*
sint8_t memBrancher( Tree *tree, TreeBranch *branch, void *ptr )
{
	quick_list *ql = (quick_list*)ptr;
	quick_list *qlPar = (quick_list*)branch->data;

	if( qlPar->ptr > ql->ptr )
		return 0;
	else if( qlPar->ptr == ql->ptr )
		return 1;
	else
		return 2;
}
*/

PointerBuffer::PointerBuffer(size_t _unit, bool _tracing)
{
	alloced=0;
	chunk=0;
	unit= _unit + MEM_PAD + ( _tracing ? sizeof(mem_trace) : sizeof(mem_track) );
	pubunit = _unit;
	tracing=_tracing;
	chunkused=0;
	chunks = NULL;
	freeSingles = NULL;
	freeGroups = NULL;
	allocations = 0;

	tables_main = mainMem;
	tsInit( &Schunks );
	tsInit( &Ssingles );
	tsInit( &Sgroups );
	tsInit( &Salloc );

	mainMem = false;
	tSingles = new DHashmap64(17293822569102704640, 16); /* fuck it's just a real big number */
	tSingles->usedivmod = true;
	tGroups = new DHashmap64(17293822569102704640, 16);
	tGroups->usedivmod = true;
//	tSingles->branchfunc = tGroups->branchfunc = memBrancher;
//	tSingles->Assert(); tGroups->Assert();
	mainMem = tables_main;

	pointerbuffers = quicklist( this, pointerbuffers );
}

PointerBuffer::~PointerBuffer()
{
	quick_list *ql;

	for( ql = pointerbuffers; ql; ql = ql->next ) {
		if( ql->ptr == this ) {
			if( ql == pointerbuffers ) pointerbuffers = ql->next;
			unquicklist(ql);
			break;
		}
	}
	ReleaseAll();
}

void PointerBuffer::ReleaseAll( void )
{
	quick_list *ql, *qln;

	// release from freeSingles and freeGroups
	// note the actual memory is still in the chunks; that's where we'll free it from.
	for( ql = freeSingles; ql; ql = qln ) {
		qln = ql->next;
		unquicklist(ql);
	}

	for( ql = freeGroups; ql; ql = qln ) {
		qln = ql->next;
		unquicklist(ql);
	}

	bool wasMain = mainMem;

	mainMem = false;
	deleteMem(tSingles);
	deleteMem(tGroups);
	mainMem = wasMain;

	fprintf(stdout, "Allocated chunks: %u\n", (unsigned)alloced);
	if( tracing ) { // clear all the allocation traces
		mem_trace *me, *men;
		for( ql = chunks; ql; ql = ql->next ) {
			for( me = (mem_trace*)ql->ptr; me; me = men ) {
				if( ql == chunks && (char*)me >= (char*)ql->ptr + chunkused )
					break;
				if( (char*)me >= (char*)ql->ptr + unit*chunk )
					break;
				men = (mem_trace*)( (char*)me + me->sz );
				if( me->sz == 0 )
					break;
				if( me->alloctrace )
					free(me->alloctrace);
				if( me->freetrace )
					free(me->freetrace);
			}
		}
	}
	uint32_t fcount=0;
	for( ql = chunks; ql; ql = qln ) {
		qln = ql->next;
		free( ql->ptr );
		unquicklist( ql );
		fcount++;
	}
	fprintf(stdout, "Freed chunks: %u\n", (unsigned)fcount);
	fflush(stdout);

	chunks = freeGroups = freeSingles = NULL;
	chunkused=0;
	allocations = 0;
	alloced=0;
}
void PointerBuffer::releaseRemainingChunk( void )
{
	if( chunk*unit == chunkused )
		return;
	char *addr = (char*)chunks->ptr + chunkused;
	//int premem_size = MEM_PAD + ( tracing ? sizeof(mem_trace) : sizeof(mem_track) );
	if( tracing ) {
		mem_trace *me = (mem_trace*)addr;
		me->refs = 0;
		me->sz = chunk*unit - chunkused;
		me->alloccount = me->freecount = 0;
		me->alloctrace = me->freetrace = NULL;
	} else {
		mem_track *mt = (mem_track*)addr;
		mt->refs = 0;
		mt->sz = chunk*unit - chunkused;
	}
	/*
	bool wasMem = mainMem;
	mainMem = false;

	if( chunk*unit - chunkused == unit ) {
		freeSingles = quicklist( addr, freeSingles );
		tSingles->Add( tSingles->pointerkey(addr), freeSingles );
	} else {
		freeGroups = quicklist( addr, freeGroups );
		tGroups->Add( tGroups->pointerkey(addr), freeGroups );
	}
	mainMem = wasMem;
	*/
}

// Releases a chunk.
void PointerBuffer::Defrag( void *ch )
{
	quick_list *ql;
	char *chStart, *chEnd;
	int foundS = 0, foundG = 0;
	tlist *within;
	tnode *n;//, *nn;
	//quick_list *qTarget;
	bool memWas = mainMem;

	//fprintf(stdout, "Releasd %p\n", ch);
	//fflush(stdout);


	chStart = (char*)ch;
	chEnd = chStart + unit*chunk;
	// release from freeSingles and freeGroups

	mainMem = false;

	tsEnter( &Ssingles );
	within = tSingles->GetRange( tSingles->pointerkey(chStart), tSingles->pointerkey(chEnd) );
	forTLIST( ql, n, within, quick_list* ) {
		if( ql->ptr >= chStart && ql->ptr <= chEnd ) {
			tSingles->Del( tSingles->pointerkey( ql->ptr ), ql );
			if( freeSingles == ql ) freeSingles = ql->next;
			unquicklist(ql);
			foundS ++;
		}
	}
	tsExit( &Ssingles );
	deleteMem(within);

/*
	for( ql = freeSingles; ql; ql = ql->next ) {
		if( ql->ptr >= chStart && ql->ptr <= chEnd ) {
			fprintf(stdout, "Missed 1!\n");
			break;
		}
	}
*/
	//uint32_t beforeCount, afterCount;

	//beforeCount = tGroups->count;
	tsEnter( &Sgroups );
	within = tGroups->GetRange( tGroups->pointerkey(chStart), tGroups->pointerkey(chEnd) );
	//fprintf(stderr, "Remove %d groups\n", within->count);
	forTLIST( ql, n, within, quick_list* ) {
		if( ql->ptr >= chStart && ql->ptr < chEnd ) {
			// yep.
			//fprintf(stderr, "Group count before delete: %lu\n", tGroups->count);
			tGroups->Del( tGroups->pointerkey( ql->ptr ), ql );
			//fprintf(stderr, "Group count after delete: %lu\n", tGroups->count);
			if( freeGroups == ql ) freeGroups = ql->next;
			unquicklist(ql);
			foundG ++;
			//fprintf(stderr, "Group count after unquicklist: %lu\n", tGroups->count);
		}
	}
	tsExit( &Sgroups );
	deleteMem(within);
	//afterCount = tGroups->count;
	//fprintf(stderr, "groups counts: %lu %lu\n", beforeCount, afterCount);
	//fflush(stderr);

/*
	for( ql = freeGroups; ql; ql = ql->next ) {
		if( ql->ptr >= chStart && ql->ptr <= chEnd ) {
			fprintf(stdout, "Missed one!\n");
			break;
		}
	}
*/


	/*
	for( ql = freeSingles; ql; ql = qln ) {
		qln = ql->next;
		if( ql->ptr >= chStart && ql->ptr < chEnd ) {
			// yep.
			if( freeSingles == ql ) freeSingles = qln;
			unquicklist(ql);
			foundS ++;
		}
	}
	for( ql = freeGroups; ql; ql = qln ) {
		qln = ql->next;
		if( ql->ptr >= chStart && ql->ptr < chEnd ) {
			// yep.
			if( freeGroups == ql ) freeGroups = qln;
			unquicklist(ql);
			foundG ++;
		}
	}
	*/

	//if( use_membug ) {
	if( foundS != 0 || foundG != 0 ) {
		//fprintf(stdout, "Defrag(): found %d singles and %d groups.\n", foundS, foundG);
		//fflush(stdout);
	}

	alloced--;

	mainMem = memWas;
}
void PointerBuffer::Defrag( void )
{
	quick_list *ql, *qln;
	mem_track *mt, *mtn;
	mem_trace *me, *men;
	bool allClear;

	tsEnter( &Salloc );

	// clear the free chunks
	for( ql = chunks ? chunks->next : NULL; ql; ql = qln ) {
		if( !ql ) break;
		qln = ql->next;

		allClear = true;
		if( tracing ) {
			for( me = (mem_trace*)ql->ptr; me; me = men ) {
				if( (char*)me >= (char*)ql->ptr + unit*chunk )
					break;
				if( ql == chunks && (char*)me >= (char*)ql->ptr + chunkused )
					break;
				men = (mem_trace*)( (char*)me + me->sz );
				if( me->sz == 0 )
					break;
				if( me->refs <= 0 ) {
					if( me->alloccount > 0 ) {
						me->alloccount=0;
						free(me->alloctrace);
					}
					continue; // already free'd
				}
				allClear = false;
				break;
			}
		} else {
			for( mt = (mem_track*)ql->ptr; mt; mt = mtn ) {
				if( (char*)mt >= (char*)ql->ptr + unit*chunk )
					break;
				if( ql == chunks && (char*)mt >= (char*)ql->ptr + chunkused )
					break;
				mtn = (mem_track*)( (char*)mt + mt->sz );
				if( mt->sz == 0 )
					break;
				if( mt->refs <= 0 ) continue; // already free'd
				allClear = false;
				break;
			}
		}
		if( allClear ) {
			void *px = ql->ptr;
			// free this group.
			tsEnter( &Schunks );
			if( ql == chunks ) chunks = ql->next;
			unquicklist( ql );
			tsExit( &Schunks );
			Defrag(px);
			free(px);
		}
	}

	tsExit( &Salloc );
}

// _Alloc(): grab a new chunk-full of units.
// the underscore indicates private
void PointerBuffer::_Alloc(void)
{
	tsEnter( &Salloc );

	if( chunks && chunkused > 0 ) {
		releaseRemainingChunk();
	}
	//int premem_size = MEM_PAD + ( tracing ? sizeof(mem_trace) : sizeof(mem_track) );
	size_t sz = (size_t)unit * (size_t)chunk;
	void *ch = malloc( sz );
	//fprintf(stdout, "Allocatd %p\n", ch);
	if( !ch ) {
		fprintf(stderr, "Out of memory/memory corruption error: Couldn't allocate %llu bytes.", sz);
		fflush(stderr);
		abort();
	}
	memset( ch, 0, sz );
	if( sz > 1000 || totalMemory % 1000 > totalMemory+sz % 1000 ) {
		totalMemory += sz;
		//fprintf(stdout, "Total memory used: %lu\n", totalMemorrrrrrrry);
		//fflush(stdout);
	} else {
		totalMemory += sz;
	}
	tsEnter( &Schunks );

	alloced ++;
	chunks = quicklist(ch, chunks);

	tsExit( &Schunks );
	chunkused=0;

	tsExit( &Salloc );
}


// _Alloc(n): grab n sequential units. Make a new chunk if necessary.
// the underscore indicates private
void *PointerBuffer::_Alloc( uint32_t number)
{
	int premem_size = MEM_PAD + ( tracing ? sizeof(mem_trace) : sizeof(mem_track) );

	if( premem_size + number * unit + chunkused >= chunk*unit ) {
		_Alloc();
		if( premem_size + number * unit + chunkused >= chunk*unit ) {
			return _AllocOne(number);
		}
	}

	uint64_t cu = chunkused;
	chunkused += premem_size + number*unit;
	return (void*) ( (char*)chunks->ptr + cu );
}

// Allocate a specifically sized chunk able to hold 'number' units.
// the underscore indicates private
void *PointerBuffer::_AllocOne(uint32_t number)
{
	tsEnter( &Salloc );

//	fprintf(stdout, "AllocOne(%d)\n", number);
	//fflush(stdout);
	int premem_size = MEM_PAD + ( tracing ? sizeof(mem_trace) : sizeof(mem_track) );
	size_t sz = unit*number + premem_size;
	void *ch = malloc( unit * number + premem_size );
	memset( ch, 0, unit*number );
	if( sz > 1000 || totalMemory % 1000 > totalMemory+sz % 1000 ) {
		totalMemory += sz;
		//fprintf(stdout, "Total memory used: %llu\n", totalMemory);
		//fflush(stdout);
	} else {
		totalMemory += sz;
	}
	tsEnter( &Schunks );

	alloced ++;
	chunks = quicklist(ch, chunks);

	tsExit( &Schunks );
	tsExit( &Salloc );

	return ch;
}

#ifndef WIN32
#include <execinfo.h>
#endif

void *PointerBuffer::Create(void)
{
	void *ptr;
	quick_list *ql;
	int premem_size = MEM_PAD + ( tracing ? sizeof(mem_trace) : sizeof(mem_track) );

	allocations++;

	//fprintf(stdout, "Create 1x %d past %d\n", unit, chunkused );
	//fflush(stdout);
	if( freeSingles ) {
		// method 2: retrieve from unused records
		tsEnter( &Ssingles );
		ptr = freeSingles->ptr;
		ql = freeSingles->next;
		unquicklist(freeSingles);
		freeSingles = ql;
		tsExit( &Ssingles );
	} else {
		if( !chunks || chunkused+unit >= chunk*unit ) {
			// allocate another chunk of records
			_Alloc();
		}
		char *sptr = (char*)chunks->ptr;
		sptr = sptr + (intptr_t)(chunkused);
		ptr = (void*)sptr;
		chunkused += unit;
	}

	void *data;
	if( tracing ) {
		mem_trace *mte = (mem_trace*)ptr;
		//int rcount=32;
		mte->istracer = true;
		mte->sz = unit;
		mte->refs = 1;

		mte->alloctrace = (void**)malloc( sizeof(void*) * 32 );
		mte->alloccount = backtrace(mte->alloctrace, 32);
		mte->allocTime = time(NULL);

		data = (void*)( ((char*)ptr) + premem_size );
	} else {
		mem_track *mt = (mem_track*)ptr;
		mt->istracer = false;
		mt->sz = unit;
		mt->refs = 1;
		data = (void*)( ((char*)ptr) + premem_size );
	}

	return data;
}


void *PointerBuffer::CreateActualSize(size_t sz)
{
	void *ptr=NULL;
	int premem_size = MEM_PAD + ( tracing ? sizeof(mem_trace) : sizeof(mem_track) );

	allocations++;

	fprintf(stdout, "CreateSz %llu from %d\n", sz, (chunk*unit-chunkused) );
	fflush(stdout);

	if( freeGroups ) {
		ptr = splitFreeGroup( sz + premem_size );
	}

	if( !ptr ) {
		if( sz >= unit*chunk ) { // allocate a whole (possibly oversize) chunk
			ptr = _AllocOne( ceil(sz/(float)unit) );
		} else {
			if( !chunks || chunkused+(sz)+premem_size >= unit*chunk ) {
				// allocate another chunk of records
				_Alloc();
			}

			char *sptr = (char*)chunks->ptr;
			sptr = sptr + (intptr_t)(chunkused);
			ptr = (void*)sptr;
			chunkused += premem_size + sz;
		}
	}

	void *data;
	if( tracing ) {
		mem_trace *mte = (mem_trace*)ptr;
		mte->istracer = true;
		mte->sz = sz + premem_size;
		mte->refs = 1;
		#ifndef WIN32
		mte->alloctrace = (void**)malloc( sizeof(void*) * 32 );
		mte->alloccount = backtrace(mte->alloctrace, 32);
		mte->allocTime = time(NULL);
		#else
		mte->alloccount = 0;
		mte->allocTime = 0;
		mte->alloctrace = NULL;
		#endif
		data = (void*)( ((char*)ptr) + premem_size );
	} else {
		mem_track *mt = (mem_track*)ptr;
		mt->istracer = false;
		mt->sz = sz + premem_size;
		mt->refs = 1;
		data = (void*)( ((char*)ptr) + premem_size );
	}

	return data;
}

void *PointerBuffer::Create(uint32_t number)
{
	void *ptr=NULL;
	int premem_size = MEM_PAD + ( tracing ? sizeof(mem_trace) : sizeof(mem_track) );

	if( number == 1 ) return Create();

	allocations+=number;

	//fprintf(stdout, "Create %d * %d from %d\n", number, unit, (chunk*unit-chunkused) );
	//fflush(stdout);
	if( freeGroups ) {
		ptr = splitFreeGroup( number*unit );
	}

	if( !ptr ) {
		if( number >= chunk ) { // allocate a whole (possibly oversize) chunk
			ptr = _AllocOne( number );
		} else {
			if( !chunks || chunkused+(number*unit) >= unit*chunk ) {
				// allocate another chunk of records
				ptr = _Alloc(number);
			} else {
				char *sptr = (char*)chunks->ptr + (intptr_t)chunkused;
				ptr = (void*)sptr;
				chunkused += unit*number;
			}
		}
	}

	void *data;
	if( tracing ) {
		mem_trace *mte = (mem_trace*)ptr;
		mte->istracer = true;
		mte->sz = unit;
		mte->refs = 1;
#ifndef WIN32
		mte->alloctrace = (void**)malloc( sizeof(void*) * 32 );
		mte->alloccount = backtrace(mte->alloctrace, 32);
		mte->allocTime = time(NULL);
		fprintf(stderr,"Got trace: %d entries\n", mte->alloccount);
		fflush(stderr);
#else
		mte->alloccount = 0;
		mte->allocTime = 0;
		mte->alloctrace = NULL;
#endif
		data = (void*)( ((char*)ptr) + premem_size );
	} else {
		mem_track *mt = (mem_track*)ptr;
		mt->istracer = false;
		mt->sz = unit;
		mt->refs = 1;
		data = (void*)( ((char*)ptr) + premem_size );
	}
	return data;
}

void PointerBuffer::Reuse( void *ptr )
{
	if( !ptr ) return;
	int premem_size = MEM_PAD + ( tracing ? sizeof(mem_trace) : sizeof(mem_track) );

	if( tracing ) {
		mem_trace *mte = (mem_trace*)( (char*)ptr-premem_size );
		mte->refs++;
	} else {
		mem_track *mt = (mem_track*)( (char*)ptr-premem_size );
		mt->refs++;
	}
}

void PointerBuffer::Release( void *ptr )
{
	if( !ptr ) return;

	int premem_size = MEM_PAD + ( tracing ? sizeof(mem_trace) : sizeof(mem_track) );
	int refc=0, refsz;
	void *realptr = ( (char*)ptr - premem_size );

	if( tracing ) {
		mem_trace *mte = (mem_trace*)( (char*)ptr-premem_size );
		if( mte->alloctrace ) {
			free(mte->alloctrace);
			mte->alloctrace = NULL;
			mte->alloccount = 0;
		}
		mte->refs--;
		refc = mte->refs;
		refsz = mte->sz;
	} else {
		mem_track *mt = (mem_track*)( (char*)ptr-premem_size );
		mt->refs--;
		refc = mt->refs;
		refsz = mt->sz;
	}

	if( refc <= 0 ) {
		bool wasMain = mainMem;
		mainMem = false;
		if( refsz == 1 ) {
			tsEnter( &Ssingles );
			freeSingles = quicklist( realptr, freeSingles );
			tSingles->Add( tSingles->pointerkey(realptr), freeSingles );
			tsExit( &Ssingles );
		} else {
			tsEnter( &Sgroups );
			freeGroups = quicklist( realptr, freeGroups );
			tGroups->Add( tGroups->pointerkey(realptr), freeGroups );
			tsExit( &Sgroups );
		}
		mainMem = wasMain;
	}
}

void *PointerBuffer::splitFreeGroup( uint32_t needSize )
{
	uint8_t *p1;
	quick_list *ql, *qln;
	mem_trace *me, *me2;
	mem_track *ms, *ms2;
	bool wasMain = mainMem;

	tsEnter( &Sgroups );

	mainMem = false;

	if( tracing ) {
		for( ql = freeGroups; ql; ql = qln ) {
			qln = ql->next;
			me = (mem_trace*)ql->ptr;

			if( me->sz == needSize ) {
				mainMem = wasMain;
				tsExit( &Sgroups );
				return (void*)me;
			} else if( me->sz >= needSize + unit ) { // splittable
				fprintf(stderr, "Splitting group(%u) into %u+%u", me->sz, needSize, me->sz-needSize);
				p1 = (uint8_t*)me;

				me2 = (mem_trace*)( p1 + ( me->sz - needSize ) );
				me2->sz = needSize;
				me->sz -= needSize;

				if( me->sz == unit ) { // move to freeSingles

					freeSingles = quicklist( p1, freeSingles );
					tSingles->Add( tSingles->pointerkey(p1), freeSingles );

					tGroups->Del( tGroups->pointerkey(p1), ql );
					if( ql == freeGroups ) freeGroups = ql->next;
					unquicklist(ql);

				}
				mainMem = wasMain;
				tsExit( &Sgroups );
				return (void*)me2;
			}
		}
	} else {
		for( ql = freeGroups; ql; ql = qln ) {
			qln = ql->next;
			ms = (mem_track*)ql->ptr;

			if( ms->sz == needSize ) {
				mainMem = wasMain;
				tsExit( &Sgroups );
				return (void*)ms;
			} else if( ms->sz >= needSize + unit ) { // splittable
				fprintf(stderr, "Splitting group(%u) into %u+%u", ms->sz, needSize, ms->sz-needSize);
				p1 = (uint8_t*)ms;

				ms2 = (mem_track*)( p1 + ( ms->sz - needSize ) );
				ms2->sz = needSize;
				ms->sz -= needSize;

				if( ms->sz == unit ) { //! move to freeSingles

					freeSingles = quicklist( p1, freeSingles );
					tSingles->Add( tSingles->pointerkey(p1), freeSingles );

					tGroups->Del( tGroups->pointerkey(p1), ql );
					if( ql == freeGroups ) freeGroups = ql->next;
					unquicklist(ql);

				}
				mainMem = wasMain;
				tsExit( &Sgroups );
				return (void*)ms2;
			}
		}
	}

	mainMem = wasMain;
	tsExit( &Sgroups );
	return NULL;
}




void memadd( char **pdest, char *psrc, int len )
{
	memcpy( *pdest, psrc, len );
	(*pdest) = (*pdest) + len;
}
void memaddstr( char **pdest, char *psrc )
{
	int len;

	len = strlen(psrc);
	memadd( pdest, (char*)&len, sizeof(int) );
	memadd( pdest, psrc, len );
}

void memsub( char **psrc, char *pdest, int len )
{
	memcpy( pdest, *psrc, len );
	(*psrc) = (*psrc) + len;
}

void memsubstr( char **psrc, char **pdest )
{
	int len;

	memsub(psrc, (char*)&len, sizeof(int));
	*pdest = (char*)grabMem(len+1);
	memsub(psrc, *pdest, len);
	(*pdest)[len] = '\0';
}
