#include "sendao.h"


DeepHash *new_deephash(void)
{
	DeepHash *p = (DeepHash*)grabMem(sizeof(DeepHash));
	p->data.ptr = NULL;
	p->key = 0;
	p->deeper = false;
	return p;
}

void free_deephash(DeepHash *x)
{
	if( x->deeper ) {
		uint8_t n;
		for(n=0;n<10;n++){//!!!
			free_deephash(x->data.ptrs[n]);
		}
		releaseMem(x->data.ptrs);
	}
	releaseMem(x);
}

DHash::DHash( uint32_t _keymax, uint16_t _layermax )
{
	keymax = _keymax;
	layermax = _layermax;

	uint32_t vx = keymax/layermax;
	for( maxdepth = 0; vx > layermax; vx /= layermax, maxdepth++ );

	maxdepth++;

	keyrates = (uint32_t*)grabMem(sizeof(uint32_t)*(maxdepth+1));

	uint8_t n = maxdepth;
	keyrates[n] = 1;
	do {
		--n;
		keyrates[n] = vx;
		vx *= layermax;
	} while( n != 0 );

	if( vx != keymax ) {
		fprintf(stderr, "Error: rebuilding for dhash did not work as expected (%u/%u)", vx, keymax);
	}

	top.data.ptr = NULL;
	top.deeper = false;
	top.key = 0;
}

DHash::~DHash()
{
	Clear();
}

void DHash::Clear(void)
{
	Recursor rc;
	DeepHash *i;
	int n;

	rc.Push( &top );
	while( (i=(DeepHash*)rc.Iterate()) ) {
		if( !i->deeper ) {
			releaseMem(i);
			continue;
		}

		for( n=0; n<layermax; ++n ) {
			rc.Push( i->data.ptrs[n] );
		}
		releaseMem(i->data.ptrs);
		if( i != &top )
			releaseMem(i);
	}
}

DeepHash *DHash::_Get( uint32_t idc, uint8_t *depth )
{
	uint32_t layerkey, lastkey=idc;
	DeepHash *i, *j;

	*depth = 0;
	for( i = &top; i; i = j ) {
		if( (*depth) > maxdepth ) {
			fprintf(stderr, "dhash::_get maxdepth exception?\n");
			return i;
		}

		layerkey = lastkey / keyrates[*depth];
		lastkey = idc % keyrates[*depth];

		if( !i->deeper ) return i;

		j = i->data.ptrs[ layerkey ];
		if( !j ) return i;

		(*depth)++;
	}

	throw "invalid code marker 2387491";
}
void *DHash::Get( uint32_t idc )
{
	uint8_t depth;
	DeepHash *x = _Get(idc, &depth);
	if( x->key == idc )
		return x->data.ptr;
	return NULL;
}


void DHash::Foreach( voidFunction *vf )
{
	Recursor rc;
	DeepHash *i;
	int n;

	rc.Push( &top );
	while( (i=(DeepHash*)rc.Iterate()) ) {
		if( !i->deeper ) {
			vf(i);
			continue;
		}

		for( n=0; n<layermax; ++n ) {
			rc.Push( i->data.ptrs[n] );
		}
		vf(i->data.ptrs);
	}
}

tlist *DHash::ToList( cloneFunction *vf )
{
	Recursor rc;
	tlist *results = new tlist;
	DeepHash *i;
	int n;

	rc.Push( &top );
	while( (i=(DeepHash*)rc.Iterate()) ) {
		if( !i->deeper ) {
			results->PushBack( vf ? vf(i) : i );
			continue;
		}

		for( n=0; n<layermax; ++n ) {
			rc.Push( i->data.ptrs[n] );
		}
	}

	return results;
}


DeepHash *new_deephash( uint32_t idc, void *v )
{
	DeepHash *result = new_deephash();
	result->data.ptr = v;
	result->key = idc;
	result->deeper = false;
	return result;
}

void DHash::Set( uint32_t idc, void *v )
{
	uint8_t depth, layern, oldlayern;
	uint32_t ratekey, oldratekey;
	DeepHash *result=NULL, *y, *x = _Get(idc, &depth);

	if( !x ) {
		fprintf(stderr,"dhash is full?\n");
		throw "dhash shouldn't fill.";
	}

	if( !x->deeper ) {
		if( x->data.ptr == NULL )
			x->key = idc;
		if( x->key == idc ) {
			x->data.ptr = v;
			return;
		}
	}

	result = new_deephash(idc,v);

	if( x->deeper ) {
		ratekey = idc % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		layern = ratekey / keyrates[depth];
		x->data.ptrs[ layern ] = result;
		return;
	}

	y=x;
	do {
		x = y;
		y = new_deephash(x->key,x->data.ptr);

		x->deeper = true;

		x->data.ptrs = (DeepHash**)grabMem(sizeof(DeepHash*)*layermax);
		memset( x->data.ptrs, 0, sizeof(DeepHash*)*layermax );

		oldratekey = y->key % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		oldlayern = oldratekey / keyrates[depth];
		x->data.ptrs[ oldlayern ] = y;

		ratekey = idc % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		layern = ratekey / keyrates[depth];

		depth++;
	} while( layern == oldlayern );

	x->data.ptrs[ layern ] = result;
}

bool DHash::Has( uint32_t idc )
{
	uint8_t depth;
	DeepHash *x = _Get(idc,&depth);
	if( x && !x->deeper ) return true;
	return false;
}

void DHash::Del( uint32_t idc )
{
	uint8_t depth;
	uint32_t layerkey, lastkey=idc;
	DeepHash *i, *j;

	depth = 0;
	for( i = &top; i; i = j ) {
		if( !i->deeper ) {
			return;
		}
		if( depth > maxdepth ) {
			fprintf(stderr, "dhash::del maxdepth exception?\n");
			return;
		}

		layerkey = lastkey / keyrates[depth];
		lastkey = idc % keyrates[depth];

		j = i->data.ptrs[ layerkey ];
		if( !j ) return;

		if( !j->deeper && j->key == idc ) {
			i->data.ptrs[layerkey] = NULL;
			free_deephash(j);
			return;
		}

		depth++;
	}

	throw "invalid code marker 2387492";
}
