#include "sendao.h"


DeepHashMap *new_deephashmap(void)
{
	DeepHashMap *p = (DeepHashMap*)grabMem(sizeof(DeepHashMap));
	p->data.ptrs = NULL;
	p->key = 0;
	p->deeper = false;
	return p;
}
DeepHashMap *new_deephashmap( uint32_t idc, void *v )
{
	DeepHashMap *result = new_deephashmap();
	result->data.vals = new tlist();
	result->data.vals->Push(v);
	result->key = idc;
	result->deeper = false;
	return result;
}

DeepHashMap *new_deephashmap( uint32_t idc, tlist *vsrc )
{
	DeepHashMap *result = new_deephashmap();
	result->data.vals = vsrc;
	result->key = idc;
	result->deeper = false;
	return result;
}

void free_deephashmap(DeepHashMap *x, uint8_t layermax)
{
	uint8_t n;
	Recursor rc;

	rc.Push(x);
	while( (x=(DeepHashMap*)rc.Iterate()) ) {

		if( x->deeper ) {
			for(n=0;n<layermax;n++){
				rc.Push( x->data.ptrs[n] );
			}
		} else {
			if( x->data.vals != NULL ) {
				x->data.vals->Clear();
				deleteMem(x->data.vals);

			}
		}
		releaseMem(x);
	}
}

DHashmap::DHashmap( uint32_t _keymax, uint16_t _layermax )
{
	keymax = _keymax;
	layermax = _layermax;
	usedivmod = false;

	uint32_t vx = keymax/layermax;
	for( maxdepth = 0; vx > layermax; vx /= layermax, maxdepth++ );

	maxdepth++;

	keyrates = (uint32_t*)grabMem(sizeof(uint32_t)*(maxdepth+1));

	uint8_t n = maxdepth;
	keyrates[n] = 1;
	do {
		--n;
		keyrates[n] = vx;
		vx *= layermax;
	} while( n != 0 );

	if( vx != keymax ) {
		fprintf(stderr, "Error: rebuilding for dhash did not work as expected (%u/%u)", vx, keymax);
	}

	top.data.ptrs = NULL;
	top.deeper = false;
	top.key = 0;
}

DHashmap::~DHashmap()
{
	Clear();
	releaseMem(keyrates);
}

void DHashmap::Clear(voidFunction *vF)
{
	uint8_t n;

	if( !top.deeper ) {

		if( top.data.vals ) {
			top.data.vals->Clear(vF);
			deleteMem(top.data.vals);

			top.data.vals = NULL;
		}

		return;
	}

	for( n=0; n<layermax; ++n ) {
		free_deephashmap( top.data.ptrs[n], layermax );
	}
	releaseMem( top.data.ptrs );
	top.data.ptrs = NULL;
	top.deeper = false;
}

tlist *DHashmap::ToList()
{
	DeepHashMap *i;
	uint8_t n;
	tlist *results = new tlist;
	Recursor r;

	r.Push(&top);
	//fprintf(stdout, "Processing dhm::tolist %p\n", &top);

	while( (i=(DeepHashMap*)r.Iterate()) ) {

		if( i->deeper ) {
			for( n=0; n<layermax; ++n ) {
				if( i->data.ptrs[n] )
					r.Push( i->data.ptrs[n] );
			}
		} else {
			//fprintf(stdout, "Add data from %p %d\n", i->data.vals, i->data.vals->count );
			results->Append( i->data.vals );
		}

	}
	//fprintf(stdout, "found results->count = %d\n", results->count);

	return results;
}

DeepHashMap *DHashmap::_Get( uint32_t idc, uint8_t *depth )
{
	uint32_t layerkey, lastkey=Keymod(idc);
	DeepHashMap *i, *j;

	*depth = 0;
	for( i = &top; i; i = j ) {
		if( !i->deeper )
			return i;

		layerkey = floor( (float) lastkey / (float)keyrates[*depth] );
		lastkey = Keymod(lastkey, /*0*/ *depth==0?keymax:keyrates[*depth-1], keyrates[*depth]);
		//fprintf(stdout, "Depth %d source %lu layer %lu last %lu\n", *depth, idc, layerkey, lastkey);
		//fflush(stdout);

		j = i->data.ptrs[ layerkey ];
		if( !j )
			return i;

		*depth = *depth + 1;
	}

	throw "invalid code marker 2387491";
}
tlist *DHashmap::Get( uint32_t idc )
{
	uint8_t depth;
	DeepHashMap *x = _Get(idc, &depth);
	if( x && x->key == idc )
		return x->data.vals;
	//fprintf(stdout, "Entry has wrong key: %lu/%lu (Depth=%d)\n", x->key, idc, depth);
	//if( x->data.vals )
	//	fprintf(stdout, "%d entries\n", x->data.vals->count);
	//fflush(stdout);
	return NULL;
}
tlist *DHashmap::GetRange( uint32_t idc0, uint32_t idc1 )
{
	tlist *results = new tlist;
	uint8_t depth;
	uint32_t layerkey, lastkey=Keymod(idc0), layerkey2, lastkey2=KeymodHigh(idc1);
	uint32_t tkey, rejectCount=0, branchCount=0;
	DeepHashMap *pTrack, *pNext;
	Recursor r;

	if( !top.deeper ) {
		// this changes things somewhat
		if( top.data.ptrs ) {
			// we still have data in there...
			fprintf(stdout, "What? No\n");
			abort();
		}
		return results; // empty
	}

	depth = 0;
	pNext=NULL;
	for( pTrack = &top; pTrack ; pTrack = pNext ) {
		if( depth > maxdepth )
			break;

		layerkey = floor( (float)lastkey / (float)keyrates[depth] );
		layerkey2 = floor( (float)lastkey2 / (float)keyrates[depth] );

		lastkey = Keymod(lastkey, depth==0?keymax:keyrates[depth-1], keyrates[depth]);
		lastkey2 = KeymodHigh(lastkey2, depth==0?keymax:keyrates[depth-1], keyrates[depth]);

		if( layerkey != layerkey2 ) {
			//fprintf(stdout, "GR: last=%lu layer=%lu\n", lastkey, layerkey);
			//fprintf(stdout, "GRX: last=%lu layer=%lu\n", lastkey2, layerkey2);
			if( layerkey2 > layerkey ) {
				for( tkey = layerkey; tkey <= layerkey2; tkey++ ) {
					r.Push( pTrack->data.ptrs[ tkey ] );
				}
			} else {
				for( tkey = layerkey2; tkey <= layerkey; tkey++ ) {
					r.Push( pTrack->data.ptrs[ tkey ] );
				}
			}

			while( (pTrack=(DeepHashMap*)r.Iterate()) ) {

				if( !pTrack->deeper ) {
					if( ( pTrack->key >= idc0 && pTrack->key <= idc1 ) || ( pTrack->key >= idc1 && pTrack->key <= idc0 ) ) {
						// either way we want it
						results->Append( pTrack->data.vals );
					} else {
						rejectCount++;
					}
				} else {
					branchCount++;
					for( tkey = 0; tkey < layermax; tkey++ ) {
						if( pTrack->data.ptrs[tkey] )
							r.Push( pTrack->data.ptrs[tkey] );
					}
				}

			}

			//fprintf(stdout, "Rejected %lu records\n", rejectCount);
			//fprintf(stdout, "Explored %lu branches\n", branchCount);
			//fflush(stdout);

			return results;
		}

		pNext = pTrack->data.ptrs[ layerkey ];
		if( !pNext )
			break;

		depth++;
	}

	return results;
}



void DHashmap::Add( uint32_t idc, void *v )
{
	uint8_t depth, layern, oldlayern;
	uint32_t ratekey, oldratekey;
	DeepHashMap *y, *x = _Get(idc, &depth);

	if( !x ) {
		fprintf(stderr,"dhash is full?\n");
		throw "dhash shouldn't fill.";
	}

	if( !x->deeper ) {
		if( x->data.vals == NULL ) {
			x->data.vals = new tlist;
			x->key = idc;
		}
		if( x->key == idc ) {
			x->data.vals->PushBack(v);
			count++;
			return;
		}
	}

	//fprintf(stdout, "New list: %p %d\n", result->data.vals, result->data.vals->count);

	if( x->deeper ) {

		if( depth == 0 ) {
			ratekey = Keymod(idc); // uint32_max, keymax )
		} else if( depth == 1 ) {
			ratekey = Keymod(idc, keymax, keyrates[0]);
		} else {
			ratekey = Keymod(idc, keyrates[depth-2], keyrates[depth-1]);
		}
		//ratekey = idc % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		layern = floor( (float)ratekey / (float)keyrates[depth] );
		if( x->data.ptrs[layern] != NULL ) { // shouldn't happen because get_ should go all the way deep
			fprintf(stdout, "Losing a pointer!\n");
		}
//		fprintf(stdout, "Add[1]\n");
		//fflush(stdout);
		x->data.ptrs[ layern ] = new_deephashmap(idc,v);
		count++;
		return;
	}

	y=x;
	do {
		if( depth > maxdepth ) {
			// just push the result into y. we're sure y is an edge node because deeper is implied == false.
			//fprintf(stdout, "Add[2] @ %p\n", y);
			if( y->deeper ) {
				fprintf(stdout, "WTF?!\n");
			}
			fflush(stdout);
			y->data.vals->PushBack( v );
			count++;
			return;
		}
		x = y;
		y = new_deephashmap(); // x->key,x->data.vals);
		y->key = x->key;
		y->data.vals = x->data.vals;
		y->deeper = false;

		x->deeper = true;

		x->data.ptrs = (DeepHashMap**)grabMem(sizeof(DeepHashMap*)*layermax);
		memset( x->data.ptrs, 0, sizeof(DeepHashMap*)*layermax );


		if( depth == 0 ) {
			oldratekey = Keymod(y->key); // uint32_max, keymax )
		} else if( depth == 1 ) {
			oldratekey = Keymod(y->key, keymax, keyrates[0]);
		} else {
			oldratekey = Keymod(y->key, keyrates[depth-2], keyrates[depth-1]);
		}
		//oldratekey = y->key % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		oldlayern = floor( (float)oldratekey / (float)keyrates[depth] );
		x->data.ptrs[ oldlayern ] = y;


		if( depth == 0 ) {
			ratekey = Keymod(idc); // uint32_max, keymax )
		} else if( depth == 1 ) {
			ratekey = Keymod(idc, keymax, keyrates[0]);
		} else {
			ratekey = Keymod(idc, keyrates[depth-2], keyrates[depth-1]);
		}
		//ratekey = idc % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		layern = floor( (float)ratekey / (float)keyrates[depth] );

		depth++;
	} while( layern == oldlayern );

	if( x->data.ptrs[layern] != NULL ) { // shouldn't happen because get_ should go all the way deep
		fprintf(stdout, "Losing a pointer2! depth=%d, max=%d\n", depth, maxdepth);
	}
	x->data.ptrs[ layern ] = new_deephashmap(idc,v);
	count++;
	//fprintf(stdout, "Add[3]\n");
	//fflush(stdout);
}

bool DHashmap::Has( uint32_t idc )
{
	uint8_t depth;
	DeepHashMap *x = _Get(idc,&depth);
	if( x && !x->deeper && x->key == idc ) return true;
	return false;
}

uint32_t DHashmap::pointerkey( void *ptr )
{
	uintptr_t x = (uintptr_t)ptr;
	uintptr_t realmax = 0xffffFFFFffffFFFF;
	double approxValue = (double)x/(double)realmax;
	uint32_t reduced = approxValue * keymax;
	return reduced;
}

uint32_t DHashmap::Keymod( uint32_t idc, uint32_t realmax, uint32_t mymax )
{
	if( mymax == 0 )
		mymax = keymax;
	if( !usedivmod )
		return idc % mymax;

	if( realmax == 0 )
		realmax = 0xfFfFfFfF; // that's 4*8=32 ones folks
	double approxValue = (double)idc/(double)realmax;
	uint32_t result = floor( approxValue * mymax );
	fprintf(stdout, "Keymod(%u) = %f (%u)\n", idc, (float)approxValue, result);
	fflush(stdout);
	return result;
}
// KeymodHigh: return the upper bound of the approximate number
uint32_t DHashmap::KeymodHigh( uint32_t idc, uint32_t realmax, uint32_t mymax )
{
	if( mymax == 0 )
		mymax = keymax;
	if( !usedivmod )
		return idc % mymax;

	if( realmax == 0 )
		realmax = 0xfFfFfFfF; // that's 4*8=32 ones folks
	float approxValue = (double)idc/(double)realmax;
	return (uint32_t) ceil( approxValue * mymax );
}

void DHashmap::Del( uint32_t idc, void *v )
{
	uint8_t depth;
	uint32_t layerkey, lastkey= Keymod(idc);
	DeepHashMap *i, *j;

	depth = 0;
	for( i = &top; i; i = j ) {
		if( !i->deeper ) {
			fprintf(stderr, "dhashmap::del couldn't go deeper\n");
			fflush(stderr);
			return;
		}
		if( depth > maxdepth ) {
			fprintf(stderr, "dhashmap::del maxdepth exception?\n");
			fflush(stderr);
			return;
		}

		layerkey = floor( (float)lastkey / (float)keyrates[depth] );
		lastkey = Keymod( lastkey, depth==0?keymax:keyrates[depth-1], keyrates[depth]);

		j = i->data.ptrs[ layerkey ];
		if( !j ) {
			fprintf(stderr, "dhashmap::del couldn't find subtree\n");
			return;
		}

		if( !j->deeper && j->key == idc ) {

			tnode *n;

			for( n = j->data.vals->nodes; n; n = n->next ) {
				if( n->data == v ) {
					j->data.vals->Pull( n );
					count = count - 1;
					//fprintf(stderr, "(delete node: count is %lu)\n", count);
					//fflush(stderr);
					break;
				}
			}

			if( j->data.vals->count <= 0 ) { // collapse the node
				i->data.ptrs[layerkey] = NULL;
				free_deephashmap(j, layermax);
			}

			return;
		}

		depth++;
	}

	throw "invalid code marker 2387493";
}
