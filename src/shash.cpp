#include "sendao.h"


// sdhash todo: allow same key multiple values
// by indexing namevoid values into lists

DeepStrHash *new_deepstrhash(void)
{
	DeepStrHash *p = (DeepStrHash*)grabMem(sizeof(DeepStrHash));
	p->data.ptr = NULL;
	p->key = 0;
	p->skey = NULL;
	p->deeper = false;
	return p;
}

void free_deepstrhash(DeepStrHash *x)
{
	if( x->deeper ) {
		uint8_t n;
		for(n=0;n<10;n++){//!!!
			free_deepstrhash(x->data.ptrs[n]);
		}
		releaseMem(x->data.ptrs);
	}
	GS->Free(x->skey);
	releaseMem(x);
}
DeepStrHash *new_deepstrhash( const char *skey, uint32_t idc, void *v )
{
	DeepStrHash *result = (DeepStrHash*)grabMem(sizeof(DeepStrHash));
	result->data.ptr = v;
	result->key = idc;
	result->skey = GS->Copy(skey);
	result->deeper = false;
	return result;
}

SDHash::SDHash( uint32_t _keymax, uint16_t _layermax )
{
	keymax = _keymax;
	layermax = _layermax;

	uint32_t vx = keymax/layermax;
	for( maxdepth = 0; vx > layermax; vx /= layermax, maxdepth++ );

	maxdepth++;

	keyrates = (uint32_t*)grabMem(sizeof(uint32_t)*(maxdepth+1));

	uint8_t n = maxdepth;
	keyrates[n] = 1;
	do {
		--n;
		keyrates[n] = vx;
		vx *= layermax;
	} while( n != 0 );

	if( vx != keymax ) {
		fprintf(stderr, "Error: rebuilding for sdhash did not work as expected (%u/%u)", vx, keymax);
	}

	top.data.ptr = NULL;
	top.deeper = false;
	top.key = 0;
}

SDHash::~SDHash()
{
	Clear();
}

void SDHash::Clear(void)
{
	Recursor rc;
	DeepStrHash *i;
	int n;

	rc.Push( &top );
	while( (i=(DeepStrHash*)rc.Iterate()) ) {
		if( !i->deeper ) {
			GS->Free(i->skey);
			releaseMem(i);
			continue;
		}

		for( n=0; n<layermax; ++n ) {
			rc.Push( i->data.ptrs[n] );
		}
		releaseMem(i->data.ptrs);
		if( i != &top ) {
			GS->Free(i->skey);
			releaseMem(i);
		}
	}
}

DeepStrHash *SDHash::_Get( uint32_t idc, const char *skey, uint8_t *depth )
{
	uint32_t layerkey, lastkey=idc;
	DeepStrHash *i, *j;

	*depth = 0;
	for( i = &top; i; i = j ) {
		if( (*depth) > maxdepth+1 ) {
			fprintf(stderr, "sdhash::get maxdepth exception?\n");
			return i;
		}

		layerkey = lastkey / keyrates[*depth];
		lastkey = idc % keyrates[*depth];

		if( !i->deeper ) return i;

		j = i->data.ptrs[ layerkey ];
		if( !j ) return i;

		(*depth)++;
	}

	throw "invalid code marker 2387491";
}
void *SDHash::Get( const char *skey )
{
	uint32_t idc = namekey(skey);
	uint8_t depth;
	DeepStrHash *x = _Get(idc, skey, &depth);
	if( !x->deeper && x->key == idc ) {
		if( GS->Compare(x->skey, skey) )
			return x->data.ptr;
	}
	return NULL;
}


void SDHash::Set( const char *skey, void *v )
{
	uint32_t idc = namekey(skey);
	uint8_t depth, layern, oldlayern;
	uint32_t ratekey, oldratekey;
	DeepStrHash *result=NULL, *y, *x = _Get(idc, skey, &depth);

	if( !x ) {
		fprintf(stderr,"sdhash is full?\n");
		throw "sdhash shouldn't fill.";
	}

	if( !x->deeper ) {
		if( x->data.ptr == NULL ) {
			x->skey = GS->Copy(skey);
			x->key = idc;
		}
		if( x->key == idc ) {
			if( GS->Compare( x->skey, skey ) ) {
				x->data.ptr = v;
				return;
			} else {
				x->data.ptr = v;
				return;
			}
		}
	}

	result = new_deepstrhash(skey,idc,v);

	if( x->deeper ) {
		ratekey = idc % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		layern = ratekey / keyrates[depth];
		x->data.ptrs[ layern ] = result;
		return;
	}

	y=x;
	do {
		x = y;
		y = new_deepstrhash(x->skey,x->key,x->data.ptr);

		x->deeper = true;

		x->data.ptrs = (DeepStrHash**)grabMem(sizeof(DeepStrHash*)*layermax);
		memset( x->data.ptrs, 0, sizeof(DeepStrHash*)*layermax );

		oldratekey = y->key % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		oldlayern = oldratekey / keyrates[depth];
		x->data.ptrs[ oldlayern ] = y;

		ratekey = idc % ( depth == 0 ? this->keymax : keyrates[depth-1] );
		layern = ratekey / keyrates[depth];

		depth++;
	} while( layern == oldlayern );

	x->data.ptrs[ layern ] = result;
}

bool SDHash::Has( const char *skey )
{
	uint32_t idc = namekey(skey);
	uint8_t depth;
	DeepStrHash *x = _Get(idc,skey,&depth);
	if( x && !x->deeper && GS->Compare(skey, x->skey) ) return true;
	return false;
}

void SDHash::Del( const char *skey )
{
	uint32_t idc = namekey(skey);
	uint8_t depth;
	uint32_t layerkey, lastkey=idc;
	DeepStrHash *i, *j;

	depth = 0;
	for( i = &top; i; i = j ) {
		if( !i->deeper ) {
			return;
		}
		if( depth > maxdepth ) {
			fprintf(stderr, "sdhash::Del maxdepth exception?\n");
			return;
		}

		layerkey = lastkey / keyrates[depth];
		lastkey = idc % keyrates[depth];

		j = i->data.ptrs[ layerkey ];
		if( !j ) return;

		if( !j->deeper && j->key == idc ) {
			i->data.ptrs[layerkey] = NULL;
			free_deepstrhash(j);
			return;
		}

		depth++;
	}

	throw "invalid code marker 2387492";
}
