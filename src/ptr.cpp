#include "sendao.h"
#include <inttypes.h>

strmap *GS = NULL;

strmap::strmap()
{
	byName = new DHashmap(46656, 36); // 36 ^ 3 for sorting alphanumerics
}

strmap::~strmap()
{
	deleteMem(byName);

}

mapstring *new_mapstring( char *Src )
{
	mapstring  *ms = (mapstring*)grabMem(sizeof(mapstring));
	ms->len = strlen(Src);
	ms->str = Src;
	ms->count = 1;
	return ms;
}
mapstring *new_mapstring1( const char *Src )
{
	mapstring  *ms = (mapstring*)grabMem(sizeof(mapstring));
	ms->len = strlen(Src);
	ms->str = (char*)grabMem(ms->len+1);
	strcpy( ms->str, Src );
	ms->count = 1;
	return ms;
}

// Search: check to see if a string is in the system and return its wrapper if so
mapstring *strmap::Search( const char *Src, uint32_t idc )
{
	if( idc == 0 ) idc = namekey(Src);
	tlist *res = byName->Get(idc);
	mapstring *x;
	tnode *n;
	size_t len = strlen(Src);

	forTLIST( x, n, res, mapstring* ) {
		if( x->str == Src ) return x;
		if( x->len == len ) {
			if( str_cmp( x->str, Src ) == 0 ) return x;
		}
	}
	return NULL;
}


// Read: consumes original source (freeing it)
char *strmap::Read( char *Src )
{
	if( !Src ) return NULL;
	uint32_t idc = namekey(Src);
	mapstring *ms = Search(Src, idc);
	if( !ms ) {
		ms = new_mapstring(Src);
		byName->Add(idc, (void*)ms);
	} else if( ms->count > 0 ) {
		ms->count++;
		if( Src != ms->str )
			releaseMem(Src);
	}
	return ms->str;
}

// UnRead: consumes original source (freeing it) .. without incrementing use counter
char *strmap::UnRead( char *Src )
{
	if( !Src ) return NULL;
	uint32_t idc = namekey(Src);
	mapstring *ms = Search(Src,idc);
	if( !ms ) {
		ms = new_mapstring(Src);
		byName->Add(idc,(void*)ms);
	} else if( ms->count > 0 ) {
		if( Src != ms->str )
			releaseMem(Src);
	}
	return ms->str;
}

// Copy: duplicates original source (leaving it alone)
char *strmap::Copy( const char *Src )
{
	if( !Src ) return NULL;
	uint32_t idc = namekey(Src);
	mapstring *ms = Search(Src,idc);
	if( !ms ) {
		ms = new_mapstring1(Src);
		ms->count = 1;
		byName->Add(idc,(void*)ms);
	} else if( ms->count > 0 ) {
		ms->count++;
	}
	return ms->str;
}

// UnCopy: duplicates original source (leaving it alone) ... without incrementing use counter
char *strmap::UnCopy( const char *Src )
{
	if( !Src ) return NULL;
	uint32_t idc = namekey(Src);
	mapstring *ms = Search(Src,idc);
	if( !ms ) {
		ms = new_mapstring1(Src);
		byName->Add(idc,(void*)ms);
	}
	return ms->str;
}


void strmap::Deref( char *Addr )
{
	releaseMem(Addr);
}
void strmap::Free( char *Addr )
{
	if( !Addr ) return;
	uint32_t idc = namekey(Addr);
	mapstring *ms = Search(Addr,idc);
	if( !ms ) {
		fprintf(stderr, "strmap string not found(%s)", Addr);
		fflush(stderr);
		releaseMem(Addr);
		return;
	}
	if( ms->count > 0 ) {
		ms->count--;
		if( ms->count == 0 ) {
			//lprintf("Debug: strmap released too many of string %p (%s)", Addr, Addr);
			byName->Del(idc, Addr);
			memset( Addr, 0, ms->len );
			releaseMem(Addr);
		}
	}
}
bool strmap::Compare( const char *A, const char *B )
{
	if( !A || !B ) return false;
	char *a = UnCopy(A);
	char *b = UnCopy(B);
	return ( a == b );
}

ptrmap *GP = NULL;

Pointer::Pointer(sint32_t itype, void *iptr)
{
	type = itype;
	addr = iptr;
	name = NULL;
	sz = 0;
	count = 1;
}

Pointer::~Pointer()
{
	GS->Free(this->name);
}
void Pointer::SetName(const char *newname)
{
	GS->Free(this->name);
	this->name = GS->Copy(newname);
	return;//true
}

void free_pointer( void *px )
{
	Pointer *y = (Pointer*)px;
	deleteMem(y);

}

/*
Pointer *Pointer::Get(const char *varname)
{
	classdef *c = LookupClass(type);
	vardef *v = LookupVar(c,varname);
	if( !c || !v ) return NULL;
	void *localaddr = (void*)( (char*)this->addr + v->offset );
	Pointer *pchild = GP->Save(localaddr, 0, v->type, varname);
	return pchild;
}
*/


ptrmap::ptrmap()
{
//	byName = new NList(pow(36,3), 128);
//	byAddr = new NList(pow(2,sizeof(void*)), 256);
//	byName = new SMap(128);
	byAddr = new DHashmap(2097152, 128);
	bytes_in_use = 0;
}

ptrmap::~ptrmap()
{
	//! clean up pointers
	byAddr->Clear( free_pointer );
	deleteMem(byAddr); byAddr=NULL;
}
uint32_t ptrmap::addrkey(void*ptr)
{
	uint32_t x = ( (uintptr_t)ptr );
	return x;
}
uint64_t ptrmap::addrkey64(void*ptr)
{
	uint64_t x = ( (uint64_t)ptr );
	return x;
}
Pointer *ptrmap::Get( void *addr )
{
	if( !addr ) return NULL;
	tlist *res = byAddr->Get( addrkey(addr) );
	tnode *n;
	Pointer *p;

	forTLIST( p, n, res, Pointer* ) {
		if( p->addr == addr ) {
			return p;
		}
	}
	return NULL;
}
void ptrmap::Del( void *addr )
{
	Pointer *x = Get( addr );
	if( !x ) {
		//lprintf("ptrmap ptr not found");
		releaseMem(addr);
		return;
	}
	if( x->addr != addr ) {
		fprintf(stderr, "Ptrmap mismatch %p %p %u %u", addr, x->addr, addrkey(addr), addrkey(x->addr));
	}
	x->count--;
	if( x->count > 0 )
		return;

	bytes_in_use -= x->sz;

	byAddr->Del( addrkey(addr), x );
	deleteMem(x);
	releaseMem(addr);
}

Pointer *ptrmap::Save( void *addr, size_t sz, sint32_t type, const char *name )
{
	Pointer *y, *yFound;

	yFound = Get(addr);
	if( yFound ) {
		yFound->count++;
		y = yFound;
	} else {
		y = new Pointer(type,addr);
		byAddr->Add( addrkey(addr), y );
		y->sz = sz;
		bytes_in_use += y->sz;
	}

	if( name )
		y->SetName(name);

	return y;
}

void *ptrmap::Alloc( int sz )
{
	void *mem = grabMem(sz);
	memset(mem,0,sz);

	/*Pointer *x = */Save(mem, sz);
	return mem;
}

void *ptrmap::Realloc( void *oldptr, int newsz )
{
	Pointer *x = Get(oldptr);
	if( !x ) {
		x = Save(oldptr);
	}

	void *mem = grabMem(newsz);
	if( (size_t)newsz > x->sz )
		memset((char*)mem+x->sz,0,newsz-x->sz);
	memcpy(mem,oldptr,x->sz);

	Del(oldptr);

	x = Save(mem, newsz);
	return mem;
}
