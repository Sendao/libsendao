#include "sendao.h"


HTable64::HTable64( uint64_t kmax )
{
	tab = (tlist**)grabMem( kmax * sizeof(tlist*) );
	for( keymax = 0; keymax < kmax; keymax ++ ) {
		tab[keymax] = NULL;
	}
	usedkeys = 0;
}
HTable64::~HTable64()
{
	Clear();
	releaseMem(tab);
}

void HTable64::Clear( voidFunction *vF )
{
	uint64_t i;

	for( i = 0; i < keymax; i++ ) {
		if( tab[i] ) {
			tab[i]->Clear(vF);
			deleteMem(tab[i]);

			tab[i] = NULL;
		}
	}
	usedkeys = 0;

}

void HTable64::CopyFrom( HTable64 *x, keyFunction64 *vK, cloneFunction *vC )
{
	tnode *n;
	void *p, *px;
	uint64_t i;
	tlist *y;

	for( i = 0; i < x->keymax; i++ ) {
		if( !x->tab[i] ) continue;
		y = x->tab[i];
		forTLIST( p, n, y, void* ) {
			px = vC(p);
			i = vK(px);
			Add(i,px);
		}
	}
}

void HTable64::CopyFrom( tlist *x, keyFunction64 *vK, cloneFunction *vC )
{
	tnode *n;
	void *p, *px;
	uint64_t i;

	forTLIST( p, n, x, void* ) {
		px = vC(p);
		i = vK(px);
		Add(i,px);
	}
}

tlist *HTable64::Slice( uint64_t i0, uint64_t imax, cloneFunction *vC, bool pointerClone )
{
	tlist *nx = new tlist;
	tnode *n;
	void *p;
	void *px;
	uint64_t i, j=0, k=0;

	for( i = 0; i < keymax; i++ ) {
		if( tab[i] ) {
			for( n = tab[i]->nodes; n; n = n->next ) {
				p = n->data;

				j++;
				if( j < i0 )
					continue;
				k++;
				if( imax != 0 && k >= imax )
					continue;

				if( vC ) {
					nx->PushBack( vC(n->data) );
				} else if( pointerClone ) {
					px = grabMem(sizeof(void*)); // basically we just dereference the pointer into a memory slot, so if it's an int, the int goes there. blah boring
					memcpy(px, &p, sizeof(void*));
					nx->PushBack(px);
				} else {
					nx->PushBack( n->data );
				}
			}
		}
	}

	return nx;
}

tlist *HTable64::ToList( cloneFunction *vC, bool pointerClone )
{
	tlist *nx = new tlist;
	tnode *n;
	void *p;
	void *px;
	uint64_t i;

	for( i = 0; i < keymax; i++ ) {
		if( tab[i] ) {
			for( n = tab[i]->nodes; n; n = n->next ) {
				p = n->data;
				if( vC ) {
					nx->PushBack( vC(n->data) );
				} else if( pointerClone ) {
					px = grabMem(sizeof(void*)); // basically we just dereference the pointer into a memory slot, so if it's an int, the int goes there. blah boring
					memcpy(px, &p, sizeof(void*));
					nx->PushBack(px);
				} else {
					nx->PushBack( n->data );
				}
			}
		}
	}

	return nx;
}


tlist *HTable64::Search( uint64_t key )
{
	uint64_t ki = key % keymax;
	if( tab[ki] && tab[ki]->count == 0 ) {
		deleteMem(tab[ki]);

		tab[ki]=NULL;
	}
	return tab[ki];
}
void HTable64::Foreach( void cb(void *) )
{
	uint64_t i;
	tnode *n, *next;

	for( i=0; i<keymax; i++ ) {
		if( !tab[i] ) continue;
		for( n = tab[i]->nodes; n; n = next ) {
			next = n->next;
			cb( n->data );
		}
	}
}
void HTable64::Foreach( uint64_t i, void cb(void *) )
{
	tnode *n, *next;

	if( !tab[i] ) return;
	for( n = tab[i]->nodes; n; n = next ) {
		next = n->next;
		cb( n->data );
	}
}
void HTable64::SetRange( uint64_t kmax )
{
	if( usedkeys != 0 ) {
		printf("SetRange on a non-empty HTable64");
		Clear();
	}
	if(tab) releaseMem(tab);
	tab = (tlist**)grabMem( kmax * sizeof(tlist*) );
	for( keymax = 0; keymax < kmax; keymax ++ ) {
		tab[keymax] = NULL;
	}
	usedkeys = 0;
}
bool HTable64::Remove( uint64_t key, void *ptr )
{
	uint64_t ki = key % keymax;
	tlist *l = tab[ki];
	if( !l ) return false;
	tnode *n = l->Pull(ptr);
	if( !n ) return false;
	deleteMem(n);

	if( l->count == 0 ) {
		deleteMem(l);

		tab[ki]=NULL;
		usedkeys--;
	}
	return true;
}
void HTable64::Add( uint64_t key, void *ptr )
{
	uint64_t ki = key % keymax;
	tlist *l = tab[ki];
	if( !l ) {
		l = tab[ki] = new tlist();
		usedkeys++;
	}
	l->PushBack( ptr );
}
void HTable64::AddList( uint64_t key, tlist *items )
{
	uint64_t ki = key % keymax;
	tlist *l = tab[ki];
	if( !l ) {
		l = tab[ki] = new tlist();
		usedkeys++;
	}
	l->Append(items);
}
