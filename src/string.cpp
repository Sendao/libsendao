#include "sendao.h"

void strip_newline( char *str )
{
	char *pFrom, *pTo;

	for( pFrom=str, pTo=str; *pFrom; pFrom++ ) {
		if( *pFrom == '\n' || *pFrom == '\r' )
			continue;
		if( pFrom != pTo )
			*pTo = *pFrom;
		pTo++;
	}
	*pTo = '\0';

	for( pFrom=str, pTo=str; *pFrom; pFrom++ ) {
		if( pTo==str && ( *pFrom == ' ' || *pFrom == '\t' ) )
			continue;
		if( pFrom != pTo )
			*pTo = *pFrom;
		pTo++;
	}
	*pTo = '\0';
	while( pTo>str && ( *(pTo-1) == ' ' || *(pTo-1) == '\t' ) ) {
		pTo--;
		*pTo='\0';
	}
}
void strip_spaces( char *str )
{
	char *pFrom, *pTo;

	for( pFrom=str, pTo=str; *pFrom; pFrom++ ) {
		if( *pFrom == '\n' || *pFrom == '\r' || *pFrom == ' ' || *pFrom == '\t' )
			continue;
		if( pFrom != pTo )
			*pTo = *pFrom;
		pTo++;
	}
	*pTo = '\0';
}

bool is_number(const char *str)
{
    const char *pStr;

    if( !(pStr=str) ) return FALSE;
	if( !*str ) return FALSE;
    if( *str == '-' )
        pStr++;

    while( *pStr )
        if( !isdigit(*pStr++) )
            return FALSE;
    return TRUE;
}
bool is_numeric(const char *c)
{
	return is_number(c);
}

bool strprefix( const char *longstr, const char *shortstr )
{
	const char *lp, *sp;

	for( lp = longstr, sp = shortstr; *sp; sp++, lp++ ) {
		if( *lp != *sp )
			return false;
	}
	return true;
}

void strexpand( char **buf, const char *add )
{
	char *tptr;

	if( !buf )
		return;

	if( !*buf ) {
		tptr = (char*)grabMem( strlen(add) + 1 );
		strcpy(tptr, add);
	} else if( !**buf ) {
		tptr = (char*)grabMem( strlen(add) + 1 );
		strcpy(tptr, add);
		releaseMem(*buf);
	} else {
		tptr = (char*)grabMem( strlen(*buf) + strlen(add) + 1 );
		strcpy(tptr, *buf);
		strcat(tptr, add);
		releaseMem(*buf);
	}

	*buf = tptr;
}

void Arg(const char *args, int n, char *argn)
{
	const char *pArgs;
	char *pArgn;
	int i;
	bool bRemainder=false;

	if( n < 0 ) {
		bRemainder = true;
		n = (0 - n) + 1;
	}

	// Eliminate leading whitespace
	for( pArgs = args; isspace(*pArgs); pArgs++ );

	// Jump to correct word
	for( i=1; i<n && *pArgs; pArgs++  ) {
		if( isspace(*pArgs) )
			i++;
	}

	// Do remainders' write to argn
	if( bRemainder ) {
		if( !*pArgs )
			*argn = '\0';
		else
			strcpy(argn, pArgs);
		return;
	}

	// Do standard write to argn
	for( pArgn=argn; *pArgs && !isspace(*pArgs); pArgs++, pArgn++ ) {
		*pArgn = *pArgs;
	}
	*pArgn = '\0';

	return;
}


void strcpysafe( char *dst, char *src )
{
	char *pB=dst, *pS=src;
	while( *pS ) {
		*pB = *pS;
		++pS;
		++pB;
	}
	*pB = '\0';
}
char *str_dup( const char *p )
{
	char *px = (char*)grabMem( strlen(p)+1 );
	if( *p )
		strcpy(px,p);
	else
		*px = '\0';
	return px;
}


char *str_n_dup(const char *str, int len)
{
	char *x=strncpy((char*)grabMem(len+1), str, len);
	x[len]=0;
	return x;
}


int str_c_cmp( const char *a, const char *b )
{
	const char *ax,*bx;
	char c,d;
	for( ax=a,bx=b; *ax || *bx; ax++, bx++ )
	{
		c = LOWER(*ax);
		d = LOWER(*bx);
		if( c != d ) return -1;
	}
	return 0;
}
int str_cn_cmp( const char *a, const char *b )
{
	const char *ax,*bx;
	char c,d;
	for( ax=a,bx=b; *ax && *bx; ax++, bx++ )
	{
		c = LOWER(*ax);
		d = LOWER(*bx);
		if( c != d ) return -1;
	}
	return 0;
}

int str_cmp( const char *a, const char *b )
{
	const char *ax,*bx;

	for( ax=a,bx=b; *ax && *bx; ax++, bx++ )
	{
		if( *ax != *bx ) return -1;
	}
	return 0;
}

const char *str_r_str(const char *src, const char *search)
{
	const char *index;
	const char *srcA, *srcB;

	for( index=src+strlen(src); index>=src; index-- ) {
		if( *index == *search ) {
			srcA=index;
			srcB=search;
			while( *srcA == *srcB ) {
				srcA++; srcB++;
				if( !*srcB ) return index;
			}
		}
	}
	return NULL;
}
int str_lev(const char *A, const char *B)
{
	const char *a, *b, *pA, *pB;
	int dist=0, sma, smb;
	bool found;

	for( a = A, b = B; *a && *b; a++, b++ ) {
		if( *a == *b ) continue;
		found=false;
		for( pA=a,sma=0; *pA; pA++,sma++ ) {
			if( *pA == *b ) {
				found=true;
				break;
			}
		}
		for( pB=b,smb=0; *pB; pB++,smb++ ) {
			if( *a == *pB ) {
				found=true;
				break;
			}
		}
		if( !found ) {
			dist += 1;
			continue;
		}
		if( sma < smb ) {
			dist += sma;
			a = pA;
		} else {
			dist += smb;
			b = pB;
		}

	}

	return dist;
}
const char *str_str(const char *src, const char *search)
{
	const char *index;
	const char *srcA, *srcB;

	for( index=src; *index; index++ ) {
		if( *index == *search ) {
			srcA=index;
			srcB=search;
			while( *srcA == *srcB ) {
				srcA++; srcB++;
				if( !*srcB ) return index;
			}
		}
	}
	return NULL;
}
char *strtoupper( const char *src )
{
	char *buf = (char*)grabMem(strlen(src)+1);
	const char *index;
	char *ptr;

	for( index=src,ptr=buf; *index; ++index ) {
		*ptr = UPPER(*index);
		ptr++;
	}
	*ptr='\0';
	return buf;
}
char *strtolower( const char *src )
{
	char *buf = (char*)grabMem(strlen(src)+1);
	const char *index;
	char *ptr;

	for( index=src,ptr=buf; *index; ++index ) {
		*ptr = LOWER(*index);
		ptr++;
	}
	*ptr='\0';
	return buf;
}
const char *str_case_str(const char *src, const char *search)
{
	const char *index;
	const char *srcA, *srcB;

	for( index=src; *index; index++ ) {
		if( LOWER(*index) == LOWER(*search) ) {
			srcA=index;
			srcB=search;
			while( *srcA == *srcB ) {
				srcA++; srcB++;
				if( !*srcB ) return index;
			}
		}
	}
	return NULL;
}
const char *str_exp(const char *src, const char *search)
{
	const char *index;
	const char *srcA, *srcB;

	if( search[strlen(search)-1] == '@' ) { // tail end
		srcA=src+strlen(src)-strlen(search);
		srcB=search;
		while( LOWER(*srcA) == LOWER(*srcB) ) {
			srcA++; srcB++;
			if( *srcB ) return src+strlen(src)-strlen(search);
		}
		return NULL;
	}

	for( index=src; *index; index++ ) {
		if( *index == *search ) {
			srcA=index;
			srcB=search;
			while( *srcA == *srcB ) {
				srcA++; srcB++;
				if( !*srcB ) return index;
			}
		}
	}
	return NULL;
}

stringbuf::stringbuf(uint16_t chunk)
{
	len=0;
	alloc_chunk = chunk;
	alloced=0;
	maxlen=0;
	p = NULL;
}
stringbuf::stringbuf(const char *src, uint16_t chunk)
{
	len=strlen(src);
	maxlen = 0;
	alloc_chunk = chunk;
	alloced = 0;
	while(alloced<=len+1)
		alloced += alloc_chunk;
	p = (char*)grabMem(alloced);
	if( len > 0 )
		strcpy(p,src);
	else if( alloced > 0 )
		*p = '\0';
}

stringbuf::~stringbuf()
{
	releaseMem(p);
}

void stringbuf::reset(void)
{
	len=0;
	if(p)
		*p='\0';
}

void stringbuf::append(const char *src)
{
	uint16_t newlen = len+strlen(src);
	expand(newlen+1);
	if( len == 0 )
		strcpy(p,src);
	else
		strcat(p,src);
	len=newlen;
}
void stringbuf::append(char c, int n)
{
	uint16_t newlen = len+n;
	expand(newlen+1);
	int x;
	for( x=len; x<newlen; x++ )
		p[x] = c;
	p[newlen] = '\0';
	len=newlen;
}
void stringbuf::expand(int target_len)
{
	if( target_len != 0 ) target_len -= alloced;
	while( target_len >= 0 ) {
		alloced += alloc_chunk;
		target_len -= alloc_chunk;
		if( maxlen!=0 && alloced > maxlen ) {
			alloced = maxlen;
			printf("Tried to expand buffer over maxlen %d\n", maxlen);
			abort();
			break;
		}
	}
	char *tmp = (char*)grabMem(alloced);
	if(p){
		strcpy(tmp,p);
		releaseMem(p);
		p=tmp;
	} else {
		p=tmp;
		*p = '\0';
	}
}
void stringbuf::printf(const char *src,...)
{
    char buf[204800];
    va_list args;

    va_start(args, src);
    ::vsprintf(buf, src, args);
    va_end(args);

	append(buf);
}
int stringbuf::vsprintf(const char *src, va_list args)
{
    char buf[204800];
    int i;

    i=::vsprintf(buf, src, args);

    if( i > 204800 ) {
    	printf("Buffer overflow.\n");
    	exit(-1);
    }

    if( i > 0 ) {
    	append(buf);
    }

	return i;
}
void stringbuf::clear(void)
{
	len=0;
	if(alloced>0)*p=0;
}
void stringbuf::replace(const char *srch, const char *repl)
{
	char *tgt;
	char *tmp;
	int srchlen = strlen(srch);
	int repllen = strlen(repl);
	int chglen = repllen-srchlen;
	int prelen;

	while( (tgt = strstr(p, srch)) != NULL ) {
		tmp = (char*)grabMem(strlen(p) + chglen + 1 );
		prelen = tgt-p;
		strncpy( tmp, p, prelen );
		strncpy( tmp+prelen, repl, repllen );
		strcpy( tmp+prelen+repllen, tgt+srchlen );

		setcontents( tmp );
		releaseMem(tmp);
	}
}

void stringbuf::setcontents( const char *newbuffer )
{
	len = strlen(newbuffer);
	expand(len+1);
	strcpy(p, newbuffer);
}

linebuf::linebuf(const char *src)
{
	lines=used=0;
	buffer=NULL;
	if( !src ) append("");
	else append(src);
}

void linebuf::setline( int lno, const char *str )
{
	while( lno >= lines ) expand();
	if( buffer[lno] ) releaseMem(buffer[lno]);
	buffer[lno] = str_dup(str);
	if( lno >= used ) used = lno;
}

void linebuf::append( const char *str )
{
	tlist *lStr = split(str, "\n");

	while( used + lStr->count > lines ) {
		expand();
	}

	char *onestr;
	tnode *n;

	forTLIST( onestr, n, lStr, char* ) {
		addline(onestr);
	}
	lStr->Clear( releaseMem );
	deleteMem(lStr);

}

void linebuf::addline( const char *str )
{
	if( used + 4 > lines ) expand();

	buffer[used] = str_dup(str);
	used++;
}

void linebuf::expand( void )
{
	char **tmp = buffer;
	lines += 128;
	buffer = (char**)grabMem( sizeof(char*)*lines );
	if( tmp ) {
		memcpy( buffer, tmp, sizeof(char*)*used );
	} else used=0;
	memset( &buffer[used], 0, sizeof(char*)*(lines-used) );
	releaseMem(tmp);
}




bitbuf::bitbuf(uint16_t chunk)
{
	len=0;
	alloc_chunk = chunk;
	alloced=0;
	while(alloced<=len)
		alloced += alloc_chunk;
	p = (char*)grabMem(alloced);
}

bitbuf::~bitbuf()
{
	releaseMem(p);
}
void bitbuf::append(char *src, int nlen)
{
	uint16_t newlen = len+nlen;
	if( alloced <= newlen ) {
		char *tmp;
		while(alloced<=newlen)
			alloced += alloc_chunk;
		tmp = (char*)grabMem(alloced);
		memcpy(tmp,p,len);
		releaseMem(p);
		p=tmp;
	}
	memcpy(p+len,src,nlen);
	len=newlen;
	p[len]=0;
}
void bitbuf::clear(void)
{
	len=0;
}


char *join( tlist *x, const char *sep )
{
	tnode *n;
	stringbuf *tgt = new stringbuf();
	char *sptr;
	bool prev=false;

	forTLIST( sptr, n, x, char* ) {
		if( prev ) {
			tgt->append(sep);
		} else {
			prev=true;
		}
		tgt->append(sptr);
	}

	sptr = str_dup(tgt->p);

	deleteMem(tgt);

	return sptr;
}

tlist *split( const char *str, const char *sep )
{
	tlist *seps = new tlist();
	seps->Push( (void*)sep );
	tlist *tgt = split(str,seps);
	deleteMem(seps);
	return tgt;
}

tlist *split( const char *str, tlist *seps )
{
	tlist *tgt = new tlist();
	const char *ptr, *lastPtr;
	tnode *n;
	const char *sep;
	char *wordBuf;
	int len,ilen;

	if( !str || !*str ) return tgt;

	//fprintf(stderr, "Split source '%s'\n", str);

	for( lastPtr = ptr = str; ptr && *ptr; ptr++ ) {
		ilen=strlen(ptr);
		forTLIST( sep, n, seps, const char* ) {
			len = strlen(sep);
			if( ilen>=len && strncmp(sep,ptr,len) == 0 ) {
				if( ptr != lastPtr ) {
					wordBuf = (char*)grabMem( (ptr-lastPtr) + 1 );
					strncpy(wordBuf,lastPtr,ptr-lastPtr);
					wordBuf[ptr-lastPtr]=0;
					//fprintf(stderr, "Split made string '%s'\n", wordBuf);
					tgt->PushBack( (void*)wordBuf );
				} else {
					//fprintf(stderr, "Split made string null\n");
					tgt->PushBack( (void*)str_dup("") );
				}
				ptr += len-1;
				lastPtr = ptr+1;
				break;
			}
		}
		if( !*ptr ) break;
	}
	if( ptr > lastPtr ) {
		wordBuf = (char*)grabMem( (ptr-lastPtr) + 1 );
		strncpy(wordBuf,lastPtr,ptr-lastPtr);
		wordBuf[ptr-lastPtr]=0;
		//fprintf(stderr, "Split finished string '%s'\n", wordBuf);
		tgt->PushBack( (void*)wordBuf );
	}
	//fflush(stderr);
	return tgt;
}
