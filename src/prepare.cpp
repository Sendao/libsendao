#include "sendao.h"

/*

libsendao::Prepare
  - used to initialize complicated systems
  - organizes tasks by prerequisites
  - can run multiple threads to optimize startup
  - task_id = prepare->task( task_handler );
  - prepare->require( task_id, require_task_id );
  - prepare->singlethread(); or prepare->threaded();

 */


void free_prepare_task( void *p )
{
	PrepareTask *px = (PrepareTask*)p;

	deleteMem(px->reqs);

	deleteMem(px->preps);

	releaseMem(px);
}

Prepare::Prepare()
{
	tasks = new HTable(32);
	topid = 0;
}

Prepare::~Prepare()
{
	tasks->Clear( free_prepare_task );
	deleteMem(tasks);

}

uint16_t Prepare::task( void * handler )
{
	PrepareTask *px = (PrepareTask*)grabMem(sizeof(PrepareTask));

	px->id = topid;
	topid++;

	px->handler = handler;
	px->reqs = new tlist;
	px->preps = new tlist;

	tasks->Add( px->id, (void*)px );

	return px->id;
}

void Prepare::require( uint16_t taskid, uint16_t requireid )
{
	PrepareTask *px1, *px2;

	tlist *srch;
	tnode *n;
	PrepareTask *px;

	srch = tasks->Search(taskid);
	px1 = NULL;
	forTLIST( px, n, srch, PrepareTask* ) {
		if( px->id == taskid ) {
			px1 = px;
			break;
		}
	}
	if( !px1 ) {
		fprintf(stderr, "require() invalid taskid");
		return;
	}

	srch = tasks->Search(requireid);
	px2 = NULL;
	forTLIST( px, n, srch, PrepareTask* ) {
		if( px->id == taskid ) {
			px2 = px;
			break;
		}
	}
	if( !px2 ) {
		fprintf(stderr, "require() invalid requireid");
		return;
	}

	px1->reqs->PushBack( px2 );
	px2->preps->PushBack( px1->reqs->last );
}

void Prepare::singlethread( void )
{
	// calculate dependency tree
	// run sequentially
}

void Prepare::threaded( void )
{
	// calculate tree
	// run all leaves
}
