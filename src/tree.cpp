#include "sendao.h"

Tree::Tree(sint8_t _keys)
{
	keys = _keys;
	branchfunc = NULL;
	valid = false;
	trunk = NULL;
	data = NULL;
	count = 0;
}

Tree::~Tree()
{
	Clear();
	valid = false;
}

void Tree::Assert(void)
{
	// verify inputs
	if( !branchfunc ) {
		return;
	}
	valid = true;
}

void Tree::Clear(voidFunction *vF)
{
	Recursor r;
	TreeBranch *tx;
	int i;

	if( trunk )
		r.Push( trunk );
	while( (tx=(TreeBranch*)r.Iterate()) ) {
		for( i=0; i<keys; i++ ) {
			if( tx->forks[i] )
				r.Push( tx->forks[i] );
		}
		if( vF )
			vF( tx->data );
		deleteMem(tx);
	}

	count = 0;

	return;
}

void Tree::ForeachStacks( voidFunction *vF )
{
	Recursor r;
	TreeBranch *tx;
	int i;

	if( trunk )
		r.Push( trunk );
	while( (tx=(TreeBranch*)r.Iterate()) ) {
		for( i=0; i<keys; i++ ) {
			if( tx->forks[i] )
				r.Push( tx->forks[i] );
		}
		vF(tx);
	}

	count = 0;

	return;
}

sint8_t Tree::Compare( TreeBranch *from, void *to )
{
	return branchfunc( this, from, to );
}

tlist *Tree::Range( void *p0, void *p1 )
{
	tlist *results = new tlist;
	tlist *procker = new tlist;

	TreeBranch *tr0 = Seek(p0);
	TreeBranch *tr1 = Seek(p1);
	TreeBranch *par0, *par1, *parCommon;

	int i;
	void *px;

	TreeBranch *seeker, *subnodes;

	par0 = tr0->parent;
	par1 = tr1->parent;

	if( par0->depth > par1->depth ) {
		while( par1->depth != par0->depth ) {
			par0 = par0->parent;
		}
	} else if( par1->depth > par0->depth ) {
		while( par0->depth != par1->depth ) {
			par1 = par1->parent;
		}
	}

	parCommon = par0->parent;


	procker->PushBack( (void*)0 );
	procker->PushBack(parCommon);

	while( procker->count > 0 ) {
		px = procker->FullPop();
		seeker = (TreeBranch*)procker->FullPop();

		if( px == NULL ) {

			for( i=0; i< keys; ++i ) {
				subnodes = seeker->forks[i];
				procker->PushBack( (void*)0 );
				procker->PushBack( subnodes );
			}

			procker->PushBack( (void*)1 );
			procker->PushBack(seeker);
		} else {
			if( branchfunc( this, seeker, p0 ) == 1 && branchfunc( this, seeker, p1 ) == 0 ) {
				results->PushBack( seeker->data );
			}
		}

	}


	 return results;
}

TreeBranch *Tree::Seek( TreeBranch *from, void *ptr, tree_branch_func *custom_bf )
{
	if( !valid ) return NULL;
	if( !custom_bf ) custom_bf = branchfunc;

	TreeBranch *branch=NULL, *b=from;

	while( b ) {
		branch = b;
		b = branch->forks[ custom_bf(this,branch,ptr) ];
	}
	return branch;
}

TreeBranch *Tree::Seek( void *ptr, tree_branch_func *custom_bf )
{
	if( !valid || !trunk ) return NULL;
	return Seek(trunk,ptr,custom_bf);
}

void Tree::Remove( void *ptr )
{
	TreeBranch *tgt = Seek(ptr);
	if( !tgt ) {
		fprintf(stderr, "Tree::Remove failed");
		return;
	}
	//! cut this branch from the tree
	TreeBranch *par = tgt->parent;
	sint8_t branchkey = branchfunc( this, par, ptr );
	par->forks[ branchkey ] = NULL;

	//! move all branches to parent by re-adding to tree
	TreeBranch *tgtSub;
	void *pxtgt;
	int i;

	for( i=0; i<keys; ++i ) {
		tgtSub = tgt->forks[i];
		if( !tgtSub ) continue;
		pxtgt = tgtSub->data;
		releaseMem(tgtSub);
		Add( pxtgt );
	}

	count--;
}
void Tree::Add( void *ptr )
{
	if( !valid ) return;

	TreeBranch *x = new TreeBranch(this);
	TreeBranch *branch=NULL, *b = trunk;
	sint8_t branchkey;
	x->depth = 0;
	x->data = ptr;

	if( !trunk ) {
		trunk = x;
		return;
	}

	while( b ) {
		branch = b;
		branchkey = branchfunc( this, branch, ptr );
		b = branch->forks[ branchkey ];
		x->depth++;
	};

	if( !branch )
		branch = trunk;
	x->parent = branch;
	branch->forks[ branchkey ] = x;

	count++;
}


TreeBranch::TreeBranch( Tree *ofTree )
:
	data( NULL )
{
	forks = (TreeBranch**)grabMem( sizeof(TreeBranch*) * ofTree->keys );
}

TreeBranch::~TreeBranch()
{
	releaseMem(forks);
}




HotTree::HotTree(Hot *_hot, sint8_t _keys) :  Tree(_keys), hot(_hot)
{
}

HotTree::~HotTree()
{
}
